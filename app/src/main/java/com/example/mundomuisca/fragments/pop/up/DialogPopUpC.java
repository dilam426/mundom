package com.example.mundomuisca.fragments.pop.up;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mundomuisca.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DialogPopUpC.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DialogPopUpC#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DialogPopUpC extends DialogFragment {

    private String tipoCostumbre;
    private ImageView imgCostumbre;
    private TextView tituloC, parrafo1, parrafo2, parrafo3, dialogC;
    private LinearLayout texto1, texto2, texto3;
    private OnFragmentInteractionListener mListener;

    public DialogPopUpC() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DialogPopUpC.
     */
    // TODO: Rename and change types and number of parameters
    public static DialogPopUpC newInstance(String param1, String param2) {
        DialogPopUpC fragment = new DialogPopUpC();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
          tipoCostumbre = getArguments().getString("costumbre");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_pop_up_c, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        imgCostumbre = view.findViewById(R.id.imgCostumbres);
        tituloC = view.findViewById(R.id.tituloCostumbres);
        parrafo1 = view.findViewById(R.id.txtParrafo1);
        parrafo2 = view.findViewById(R.id.txtParrafo2);
        parrafo3 = view.findViewById(R.id.txtParrafo3);
        texto1 = view.findViewById(R.id.parrafo1);
        texto2 = view.findViewById(R.id.parrafo2);
        texto3 = view.findViewById(R.id.parrafo3);
        dialogC = view.findViewById(R.id.dialogC);

        dialogC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarDialogC();
            }
        });

        switch (tipoCostumbre){
            case "agricultura":
                imgCostumbre.setImageResource(R.drawable.agriculturainfo);
                tituloC.setText("AGRICULTURA MUISCA");
                parrafo1.setText("Los Muiscas se alimentaban con: maíz, papa, quinua, sal, miel, frutas, granos.");
                parrafo2.setText("Sus cultivabos eran de maíz, papa, quinua y algodón, entre otros productos agrícolas.");
                parrafo3.setText("Esta cultura no llegó a domesticar animales; sin embargo, la caza y la pesca era parte de su rutina. Animales como los venados, conejos, aves y diversa clase de pescados conformaban su dieta alimenticia.");
                break;
            case "aspecto":
                imgCostumbre.setImageResource(R.drawable.fisicomuisca);
                tituloC.setText("ASPECTO FÍSICO MUISCA");
                parrafo1.setText("Los Muiscas se caracterizaban por los siguientes rasgos fisicos: mediana estatura, piel morena, pelo negro y liso, mejillas salientes, nariz ancha, boca grande, dientes grandes y parejos, ojos negros, eran fuertes y musculosos.");
                texto2.setVisibility(View.GONE);
                texto3.setVisibility(View.GONE);
                break;
            case "orfebreria":
                imgCostumbre.setImageResource(R.drawable.orfebrer_amuisca);
                tituloC.setText("ORFEBRERÍA MUISCA");
                parrafo1.setText("Los Muiscas eran excelentes orfebres, practicaban el trueque de mantas, sal, cerámicas, coca y esmeraldas con los pueblos vecinos. ");
                parrafo2.setText("Ellos fabricaban figurillas y objetos de adorno, como diademas, collares, narigueras, pulseras, pectorales, máscaras y los denominados tunjos, decorados con hilos de oro y, en general, figuras antropomorfas (Imagenes elaboradas con rasgos humanos) y zoomorfas (Esculturas realizadas con rasgos de animales) planas.");
                texto3.setVisibility(View.GONE);
                break;
            case "vestimenta":
                imgCostumbre.setImageResource(R.drawable.vestimentacos);
                tituloC.setText("VESTIMENTA Y ADORNOS MUISCAS");
                parrafo1.setText("Los Muiscas se vestían con mantas de algodón que ellos mismos tejían. Se abrochaban las mantas en el hombro con alfileres de oro y otras veces con cordones coloreados, hechos con el mismo algodón. ");
                parrafo2.setText("Esta cultura tenía la costumbre de pintarse la cara con colores rojo y negro, también se pintaban los dientes de negro; entre las joyas de oro que solían usar son: pectorales, collares de dientes de tigre o dientes de humanos, pulseras de oro y plata y también usaban narigueras. ");
                texto3.setVisibility(View.GONE);
                break;

        }

        return view;
    }

    public void cerrarDialogC(){
        this.dismiss();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

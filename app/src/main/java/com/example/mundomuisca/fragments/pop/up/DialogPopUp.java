package com.example.mundomuisca.fragments.pop.up;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.mundomuisca.R;

public class DialogPopUp extends DialogFragment {

    private String dato, tipo,laguna;
    private ImageView ubicacionMapa, iconUb;
    private TextView infoCacique, tituloDepto, tvUbi;
    private TextView tvCiudad1, tvCiudad2, tvCiudad3, tvCiudad4, tvCiudad5;
    private LinearLayout ciudad2, ciudad3, ciudad6, ciudadesFila1,ciudadesFila2, linearUbicacion;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null){
            tipo = getArguments().getString("tipoPopUp");
            dato = getArguments().getString("departamento");
            laguna = getArguments().getString("laguna");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.dialog_pop_up, container,false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ubicacionMapa = view.findViewById(R.id.ubicacionIndigena);

        tituloDepto = view.findViewById(R.id.tituloDepto);
        infoCacique = view.findViewById(R.id.infoCacique);
        tvUbi = view.findViewById(R.id.tvUbi);
        iconUb = view.findViewById(R.id.iconUb);

        //Linear de Ciudades
        ciudad2 = view.findViewById(R.id.ciudad2);
        ciudad3 = view.findViewById(R.id.ciudad3);
        ciudad6 = view.findViewById(R.id.ciudad6);
        linearUbicacion = view.findViewById(R.id.linearUbicacion);
        ciudadesFila1 = view.findViewById(R.id.ciudadesFila1);
        ciudadesFila2 = view.findViewById(R.id.ciudadesFila2);

        //TextView de Ciudades
        tvCiudad1 = view.findViewById(R.id.tvCiudad1);
        tvCiudad2 = view.findViewById(R.id.tvCiudad2);
        tvCiudad3 = view.findViewById(R.id.tvCiudad3);
        tvCiudad4 = view.findViewById(R.id.tvCiudad4);
        tvCiudad5 = view.findViewById(R.id.tvCiudad5);

        switch (tipo){
            case "ubicacion":
                if (dato.equals("santander")){
                    ubicacionMapa.setImageResource(R.drawable.indigena);
                    tituloDepto.setText("SANTANDER");
                    tvCiudad1.setText("Guanentá");
                    ciudad2.setVisibility(view.GONE);
                    ciudad3.setVisibility(view.GONE);
                    ciudadesFila2.setVisibility(view.GONE);
                    infoCacique.setText("La tribu que habitaba en el Sur de Santander, era una población declarada como indepencdiente, es decir, no era gobernada por un cacique.");


                }else if (dato.equals("boyaca")){
                    ubicacionMapa.setImageResource(R.drawable.indiapop);
                    tituloDepto.setText("BOYACÁ");
                    tvCiudad1.setText("Duitam");
                    tvCiudad2.setText("Tenza");
                    tvCiudad3.setText("Tunja");
                    tvCiudad4.setText("Sogamoso");
                    tvCiudad5.setText("Chiquinquirá");
                    ciudad6.setVisibility(view.GONE);
                    infoCacique.setText("La tribu que habitaba el departamento de Boyacá era encabezada por un cacique denominado ZAQUE que gobernaba desde la capital en Hunza (Tunja).");
                }
                break;
            case "lagunas":
                iconUb.setVisibility(View.GONE);
                tvUbi.setVisibility(View.GONE);
                linearUbicacion.setVisibility(View.GONE);
                ciudadesFila1.setVisibility(View.GONE);
                ciudadesFila2.setVisibility(View.GONE);
                switch (laguna){
                    case "guatavita":
                        ubicacionMapa.setImageResource(R.drawable.lagunaguatavita);
                        tituloDepto.setText("LAGUNA DE GUATAVITA");
                        infoCacique.setText("Fue una de las lagunas más sagradas para los Muiscas, puesto que allí se realizaba el ritual de investidura del nuevo Zipa (Cacique). Además en esta laguna navega la leyenda El Dorado.");
                        break;
                    case "martos":
                        ubicacionMapa.setImageResource(R.drawable.lagunam);
                        tituloDepto.setText("LAGUNA DE MARTOS");
                        infoCacique.setText("Esta laguna ya no existe, ahora es un pantano con cortos y pocos cuerpos de agua, resultado de la intervención del holandés Gozalo Linnus Martos, quien explotó minas, al enterarse de los posibles tesoros que los indígenas arrojaban al agua.");
                        break;
                    case "siecha":
                        ubicacionMapa.setImageResource(R.drawable.lagunasiecha);
                        tituloDepto.setText("LAGUNA DE SIECHA");
                        infoCacique.setText("Durante la época colonial esta laguna fue sometida a procesos de drenaje, con el fin de extraer piezas de oro. En 1856 fue encontrada una pieza de oro con forma de balsa que representaba a la mítica ceremonia. Dicha balsa fue nombrada “La balsa de Siecha”.");
                        break;
                    case "teusaca":
                        ubicacionMapa.setImageResource(R.drawable.lagunateusac_);
                        tituloDepto.setText("LAGUNA DE TEUSACÁ");
                        infoCacique.setText("Es el cuarto lugar de devoción, de la que se dice que también tenía grandes tesoros, entre ellos dos caimanes de oro, además de muchas joyas.");
                        break;
                    case "ubaque":
                        ubicacionMapa.setImageResource(R.drawable.lagunaubaque);
                        tituloDepto.setText("LAGUNA DE UBAQUE");
                        infoCacique.setText("Es el último altar, en ella reposan tunjos de oro y joyas con pedrerías finas. De sus aguas surgieron leyendas como las del Mohán y barba blanca.");
                        break;
                }
                break;
        }
        return view;
    }
}

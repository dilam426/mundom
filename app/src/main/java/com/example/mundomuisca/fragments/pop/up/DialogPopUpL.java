package com.example.mundomuisca.fragments.pop.up;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mundomuisca.R;


public class DialogPopUpL extends DialogFragment {

    private String tipoLaguna;
    private ImageView imgLaguna;
    private TextView tituloLaguna, infoLaguna, dialogL;


    private OnFragmentInteractionListener mListener;

    public DialogPopUpL() {
        // Required empty public constructor
    }


    public static DialogPopUpL newInstance(String param1, String param2) {
        DialogPopUpL fragment = new DialogPopUpL();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            tipoLaguna = getArguments().getString("laguna");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dialog_pop_up_l, container, false);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        imgLaguna = view.findViewById(R.id.imgLaguna);
        tituloLaguna = view.findViewById(R.id.tituloLaguna);
        infoLaguna = view.findViewById(R.id.infoLaguna);
        dialogL = view.findViewById(R.id.dialogL);

        dialogL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarDialogL();
            }
        });

        switch (tipoLaguna){
            case "guatavita":
                imgLaguna.setImageResource(R.drawable.lagunaguatavita);
                tituloLaguna.setText("LAGUNA GUATAVITA");
                infoLaguna.setText("Fue una de las lagunas más sagradas para los Muiscas, puesto que allí se realizaba el ritual de investidura del nuevo Zipa (Cacique). Además en esta laguna navega la leyenda El Dorado.");
                break;

            case "martos":
                imgLaguna.setImageResource(R.drawable.guasca);
                tituloLaguna.setText("LAGUNA DE GUASCA");
                infoLaguna.setText("Esta laguna ya no existe, ahora es un pantano con pocos charcos de agua, resultado de la intervención del holandés Gozalo Linnus Martos, quien explotó minas, al enterarse de los posibles tesoros que los indígenas arrojaban al agua; debido a esto esta laguna también es conocida como “La Laguna de Martos”.");
                break;

            case "siecha":
                imgLaguna.setImageResource(R.drawable.lagunasiecha);
                tituloLaguna.setText("LAGUNA DE SIECHA");
                infoLaguna.setText("Durante la época colonial esta laguna fue sometida a procesos de drenaje, con el fin de extraer piezas de oro. En 1856 fue encontrada una pieza de oro con forma de balsa que representaba a la mítica ceremonia. Dicha balsa fue nombrada “La balsa de Siecha”.");
                break;

            case "teusaca":
                imgLaguna.setImageResource(R.drawable.lagunateusac_);
                tituloLaguna.setText("LAGUNA DE TEUSACÁ");
                infoLaguna.setText("Es el cuarto lugar de devoción, de la que se dice que también tenía grandes tesoros, entre ellos dos caimanes de oro, además de muchas joyas.");
                break;

            case "ubaque":
                imgLaguna.setImageResource(R.drawable.lagunaubaque);
                tituloLaguna.setText("LAGUNA DE UBAQUE");
                infoLaguna.setText("Es el último altar, en ella reposan tunjos de oro y joyas con pedrerías finas. De sus aguas surgieron leyendas como las del Mohán y barba blanca.");
                break;

        }


        return view;
    }

    public void cerrarDialogL(){
        this.dismiss();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

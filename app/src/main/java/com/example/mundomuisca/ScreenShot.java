package com.example.mundomuisca;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class ScreenShot {
    private Activity activity;
    private Context context;

    public ScreenShot(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;
    }

    public void ScreenShott(View view){
        View v1 = activity.getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);

        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

        String filePath = Environment.getExternalStorageDirectory()+"/Download/"+ Calendar.getInstance().getTime().toString()+".jpg";
        File fileScreenShot = new File(filePath);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(fileScreenShot);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent ayuda = new Intent(Intent.ACTION_SEND);
        Uri uri = Uri.fromFile(fileScreenShot);
        ayuda.setType("image/jpg");
        ayuda.putExtra(Intent.EXTRA_STREAM, uri);
        ayuda.setPackage("com.whatsapp");

        try {
            activity.startActivity(ayuda);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
        }
    }
}

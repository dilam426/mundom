package com.example.mundomuisca.cuestionarios.mitologia.fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.cuestionarios.mitologia.MitologiaActivity;
import com.example.mundomuisca.cuestionarios.mitologia.preguntas.QuizMActivity;
import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.toolbar.Toolbar;
import com.google.android.material.tabs.TabLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class InstruccionesMitos extends AppCompatActivity {

    private TextView tvMonedas, tvVidas;
    private Toolbar toolbar;
    public ViewPager pantallaPager;
    public ViewPagerAdapter viewPagerAdapter;
    private ArrayList<ItemsPantallaMitos> listaVideos;
    public TabLayout tabLayout;
    public Button btnSiguiente, btnQuizVideos;
    public int posicion;
    public String lecChiminigagua = "En el principio el dios Chiminigagua creó a los cuerpos para iluminar todo lo que existía; la entidad llamada Bachué. En ella, Chiminigagua como un rayo en forma de serpiente, descendió del sol a la tierra para dar origen a los padres de la humanidad.";
    public String lecBachue = "Bachué y su joven acompañante, hijos de la sagrada laguna de Iguaque y el sol, por ser los primeros habitantes de la tierra debieron aprender a vivir de ésta.\n" +
            "-- ¿Qué haces Bachué?\n" +
            "-- Busco refugio.\n" +
            "Con el tiempo aprendieron a trabajar las piedras, manipular el fuego, recolectar y cazar sus alimentos; habilidades que transmitieron a sus hijos, los muiscas. Una vez enseñaron sus conocimientos, ellos partieron para continuar poblando y educando sus hijos muiscas. A quienes dejaron en paz y armonía hasta que de la mano de chía, la deidad del caos y el mal cayó la oscuridad.\n";
    public String lecGoranchacha = "Una época liderada por Goranchacha, primero de los grandes jefes o zaques de las tribus muiscas. Goranchacha, el déspota, esclavizo los pueblos, instauró la práctica de la magia negra, pervirtió a la gente y gobernó hasta el día que divisó una tormenta que se avecinaba en el norte por lo que entre llamas y humo abandonó a sus súbditos.\n";
    public String lecChibchacum = "Tormenta desatada por Chibchacum dios de la tierra, quien enfurecido al ver la corrupción entre los muiscas, los castigó inundando la sabana de Bogotá.\n";
    public String lecBochica = "Al escuchar sus súplicas, Bochica dios benefactor de los muiscas se apiadó del hombre. Creó el salto del Tequendama para liberar a los hombres de las aguas y camino entre la gente. A su paso Bochica trajo un nuevo orden. Enseñó a los muiscas la agricultura, la metalurgia, el arte de tejer y regalo a la humanidad el maíz, la semilla de una nueva era. Luego confronto a chía por haber sembrado la esmeralda del mal y la castigó desterrandola y condenandola a iluminar la noche por el resto de la eternidad. Bochica prosiguió su camino y dejó su huella entre las tribus que bajo la influencia de sus enseñanzas hicieron florecer la civilización muisca.\n" +
            "Satisfechos por el progreso de sus hijos y tras haber recorrido y poblado el mundo, Bachué y su acompañante retornaron a su lugar de origen, la sagrada laguna de Iguaque, desde donde se despidieron del mundo al que dejaron como legado: La Cultura Muisca.";
    ImageView imgToolbar;
    Sonidos sonidos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instrucciones_mitos);

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);
        toolbar = new Toolbar(this,tvMonedas,tvVidas);
        imgToolbar = findViewById(R.id.logoMuisca);
        imgToolbar.setImageResource(R.drawable.back);
        sonidos = new Sonidos(this);
        toolbar.cargarLogros();


        //Referencias de elementos
        btnSiguiente = findViewById(R.id.btnSiguiente);
        tabLayout = findViewById(R.id.tab_indicador);
        listaVideos = new ArrayList<>();

        llenarArreglo();

        pantallaPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(this, listaVideos);
        pantallaPager.setAdapter(viewPagerAdapter);
        clicBtnSiguiente();
        tabLayout.setupWithViewPager(pantallaPager);


        imgToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volverIntro();
            }
        });



    }

    public void volverIntro(){
        sonidos.sonidoClic();
        Intent intent = new Intent(this, MitologiaActivity.class);
        startActivity(intent);
    }

    //Instancias ItemsPantallaMitos para poblar arreglo que será manejado por la clase ViewPagerAdapter con el fin de convertir los objetos a ViewPager.
    public void llenarArreglo(){
        listaVideos.add(new ItemsPantallaMitos("android.resource://" + getPackageName() + "/" + R.raw.chimini, "CHIMINIGAGUA", lecChiminigagua));
        listaVideos.add(new ItemsPantallaMitos("android.resource://" + getPackageName() + "/" + R.raw.bachue, "BACHUE", lecBachue));
        listaVideos.add(new ItemsPantallaMitos("android.resource://" + getPackageName() + "/" + R.raw.goranchacha, "GORANCHACHA",lecGoranchacha));
        listaVideos.add(new ItemsPantallaMitos( "android.resource://" + getPackageName() + "/" + R.raw.chibchacum, "CHIBCHACUM",lecChibchacum));
        listaVideos.add(new ItemsPantallaMitos( "android.resource://" + getPackageName() + "/" + R.raw.bochica, "BOCHICA",lecBochica));

    }



    //Método que permite el desplazamiento de las pantallas de los mitos
    public void clicBtnSiguiente(){
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                posicion = pantallaPager.getCurrentItem();
                if(posicion < listaVideos.size()){
                    posicion++;
                    pantallaPager.setCurrentItem(posicion);
                }if(posicion == listaVideos.size()-1) {
                    btnSiguiente.setText("Iniciar Quiz");
                    btnSiguiente.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            llamarQuiz();
                        }
                    });
                }
            }
        });
    }

    private void actualizarBoton(int p){
        if (p == 2) {
            Toast.makeText(this, "AcBtn", Toast.LENGTH_SHORT).show();
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Toast.makeText(InstruccionesMitos.this, "Pagina" + position, Toast.LENGTH_SHORT).show();
            actualizarBoton(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void llamarQuiz(){
        Intent intent = new Intent(this, QuizMActivity.class);
        startActivity(intent);
        this.finish();
    }
}

package com.example.mundomuisca.cuestionarios.mitologia.preguntas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.menu.MenuActivity;

public class SiguienteMActivity extends AppCompatActivity {

    private int tipo, numPregunta, counterM, numAciertosM;
    TextView tvCounter, tvMsgPregunta;
    SharedPreferences.Editor validadorM;
    private Button btnContinuar;
    private boolean validarAcierto;
    LinearLayout linearRecompensas, linearLogro;

    private SharedPreferences persistencia_mitos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siguiente_m);

        tvCounter = findViewById(R.id.contador);
        tvMsgPregunta = findViewById(R.id.msgPregunta);
        btnContinuar = findViewById(R.id.btnContinuar);
        linearRecompensas = findViewById(R.id.linearRecompensas);
        linearLogro = findViewById(R.id.linearLogro);

        linearRecompensas.setVisibility(View.GONE);
        linearLogro.setVisibility(View.GONE);

        //TODO: VARIABLES PARA VALIDAR ACIERTO O ERROR DE LA PREGUNTA
        Intent intent = getIntent();
        tipo = intent.getIntExtra("tipo", 0);
        validarAcierto = intent.getBooleanExtra("validarAcierto", false);

        //TODO: VARIABLES PARA LLEVAR CONTEO DE ACIERTOS
        persistencia_mitos = getSharedPreferences("PERSISTENCIA_MITOS", Context.MODE_PRIVATE);
        numAciertosM = persistencia_mitos.getInt("aciertosM", 0);
        validadorM = persistencia_mitos.edit();

        //TODO: VALIDAR SI LA RESPUESTA FUE CORRECTA O INCORRECTA
        if (validarAcierto) {
            tvMsgPregunta.setText("RESPUESTA CORRECTA!");
            numAciertosM = numAciertosM + 1;
            validadorM.putInt("aciertosM", numAciertosM);
            validadorM.commit();
        } else {
            tvMsgPregunta.setText("RESPUESTA INCORRECTA!");
        }

        //TODO: VARIABLES PARA LLEVAR EL NUMERO DE PREGUNTAS RESPONDIDAS
        counterM = persistencia_mitos.getInt("counterPreguntasM", 1);
        tvCounter.setText(counterM + "/10");
        if (counterM == 10) {
            if (numAciertosM > 5) {
                SharedPreferences logroU = getSharedPreferences("logros", Context.MODE_PRIVATE);
                SharedPreferences.Editor upLogro = logroU.edit();
                upLogro.putBoolean("logroM", true);
                upLogro.commit();
                tvMsgPregunta.setText("Felicidades!!! " + numAciertosM + "/10");
                linearRecompensas.setVisibility(View.VISIBLE);
                linearLogro.setVisibility(View.VISIBLE);
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(true);
                    }
                });
            } else {
                tvMsgPregunta.setText("Intentalo de nuevo " + numAciertosM + "/10");
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(false);
                    }
                });
            }
        }

        SharedPreferences.Editor upCounterPU = persistencia_mitos.edit();
        //TODO: CAMBIAR PARAMETRO PARA QUE CUENTE EL NUMERO DE PREGUNTAS
        counterM = counterM + 1;
        upCounterPU.putInt("counterPreguntasM", counterM);
        upCounterPU.apply();
    }

    public void siguientePreguntaM(View view){
        Intent siguiente;
        switch (tipo){
            case 1:
                numPregunta = persistencia_mitos.getInt("multipleMitos", 1);
                if (numPregunta <= 3) {
                    siguiente = new Intent(this, QuizMActivity.class);
                    siguiente.putExtra("tipo", 2);
                    startActivity(siguiente);
                    this.finish();
                }else {
                    siguiente = new Intent(this, QuizMActivity.class);
                    siguiente.putExtra("tipo", 3);
                    startActivity(siguiente);
                    this.finish();
                }
                break;
            case 2:
                numPregunta = persistencia_mitos.getInt("imagenMitos", 1);
                if (numPregunta <= 2) {
                    siguiente = new Intent(this, QuizMActivity.class);
                    siguiente.putExtra("tipo", 3);
                    startActivity(siguiente);
                    this.finish();
                }else {
                    siguiente = new Intent(this, QuizMActivity.class);
                    siguiente.putExtra("tipo", 4);
                    startActivity(siguiente);
                    this.finish();
                }
                break;
            case 3:
                siguiente = new Intent(this, QuizMActivity.class);
                siguiente.putExtra("tipo", 4);
                startActivity(siguiente);
                this.finish();
                break;
            case 4:
                numPregunta = persistencia_mitos.getInt("completarMitos", 1);
                if (numPregunta <= 2) {
                    siguiente = new Intent(this, QuizMActivity.class);
                    siguiente.putExtra("tipo", 1);
                    startActivity(siguiente);
                    this.finish();
                }else {
                    siguiente = new Intent(this, QuizMActivity.class);
                    siguiente.putExtra("tipo", 2);
                    startActivity(siguiente);
                    this.finish();
                }
                break;
        }
    }

    private void finalizarQuizU(boolean completado){
        Intent intent = new Intent(this, MenuActivity.class);
        SharedPreferences recompensas = getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorRecompensas = recompensas.edit();
        if (completado) {
            int monedasActuales = recompensas.getInt("monedas", 100);
            int vidasActuales = recompensas.getInt("vidas", 10);
            editorRecompensas.putInt("monedas", monedasActuales + 100);
            editorRecompensas.putInt("vidas", vidasActuales + 3);
            editorRecompensas.putBoolean("costumbres", true);
        }

        SharedPreferences.Editor editor = persistencia_mitos.edit();
        editor.putInt("completarMitos",1);
        editor.putInt("multipleMitos",1);
        editor.putInt("imagenMitos",1);
        editor.putInt("trueMitos",1);
        editor.putInt("counterPreguntasM", 1);
        validadorM.putInt("aciertosM", 0);
        validadorM.apply();
        editor.apply();
        editorRecompensas.apply();
        this.finish();
        startActivity(intent);
    }
}

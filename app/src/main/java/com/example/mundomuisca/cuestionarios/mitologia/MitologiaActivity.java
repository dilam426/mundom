package com.example.mundomuisca.cuestionarios.mitologia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.toolbar.Toolbar;
import com.example.mundomuisca.cuestionarios.mitologia.fragments.InstruccionesMitos;


public class MitologiaActivity extends AppCompatActivity {

    TextView tvMonedas, tvVidas;
    Toolbar toolbar;
    ImageView imgToolbar;
    Sonidos sonidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mitologia);

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);
        toolbar = new Toolbar(this,tvMonedas,tvVidas);
        imgToolbar = findViewById(R.id.logoMuisca);
        imgToolbar.setImageResource(R.drawable.back);
        sonidos = new Sonidos(this);
        toolbar.cargarLogros();

        imgToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volverMenu();
            }
        });


    }

    public void quizMitos(View view) {
        Intent quiz = new Intent(this, InstruccionesMitos.class);
        startActivity(quiz);
        finish();
    }

    public void volverMenu(){
        sonidos.sonidoClic();
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        this.finish();
    }



}

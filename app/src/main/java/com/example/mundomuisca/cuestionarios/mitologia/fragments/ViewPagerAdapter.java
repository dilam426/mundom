package com.example.mundomuisca.cuestionarios.mitologia.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import com.example.mundomuisca.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<ItemsPantallaMitos> itemsVideos;


    public ViewPagerAdapter(Context context, ArrayList<ItemsPantallaMitos> itemsVideos) {
        this.context = context;
        this.itemsVideos = itemsVideos;
    }

    //Método que crea un objeto ItemsPantallaMitos a ViewPager
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View layoutPantalla = layoutInflater.inflate(R.layout.pantalla_mitos, null);

        //Referencia a los elemento de la interfaz de pantalla_mitos.xml
        TextView titulo = layoutPantalla.findViewById(R.id.txtTituloMito);
        final VideoView videoMito = layoutPantalla.findViewById(R.id.videoMito);
        TextView lectura = layoutPantalla.findViewById(R.id.txtLecturaMito);
        FloatingActionButton play = layoutPantalla.findViewById(R.id.playVideo);
        FloatingActionButton stop = layoutPantalla.findViewById(R.id.stopVideo);

        //Asociar los elementos con los atributos de la clase ItemsPantallaMitos.java
        titulo.setText(itemsVideos.get(position).getTitulo());
        lectura.setText(itemsVideos.get(position).getLectura());
        videoMito.setVideoPath(itemsVideos.get(position).getVideo());
        Uri uri = Uri.parse(itemsVideos.get(position).getVideo());
        videoMito.setVideoURI(uri);
        /*MediaController mediaController = new MediaController(context);
        mediaController.setAnchorView(videoMito);
        videoMito.setMediaController(mediaController);*/
        videoMito.seekTo( 10 );

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoMito.start();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoMito.pause();
            }
        });


        container.addView(layoutPantalla);

        return layoutPantalla;
    }

    //Retorna el numero de objetos ViewPager según la longitud del arreglo itemsVideos
    @Override
    public int getCount() {
        return itemsVideos.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}

package com.example.mundomuisca.cuestionarios.lagunas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.toolbar.Toolbar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TrueFalseLFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TrueFalseLFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrueFalseLFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    SharedPreferences persistencia_lagunas;

    private ImageView imagenPreg;
    private Button opcVerdadera, opcFalsa;
    private TextView txtPregunta, timer;
    private int numPreguntaT;
    private CountDownTimer countDownTimer;

    private Sonidos sonidos;

    TextView tvVidas, tvMonedas;
    Toolbar toolbar;
    int restaVida;

    private OnFragmentInteractionListener mListener;

    public TrueFalseLFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TrueFalseLFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TrueFalseLFragment newInstance(String param1, String param2) {
        TrueFalseLFragment fragment = new TrueFalseLFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_true_false_l, container, false);

        //Creacion de toolbar
        tvVidas = ((QuizLActivity) getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((QuizLActivity) getActivity()).findViewById(R.id.editTextMonedas);
        toolbar = new Toolbar(getContext(), tvMonedas, tvVidas);

        sonidos = new Sonidos(getContext());

        //Variable para validar el numero de pregunta
        persistencia_lagunas = getActivity().getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
        numPreguntaT = persistencia_lagunas.getInt("trueLagunas", 1);

        timer = view.findViewById(R.id.timer);

        timer.setText(String.valueOf(30));

        txtPregunta = view.findViewById(R.id.txtPreguntaT);
        imagenPreg = view.findViewById(R.id.imagenPregunta);
        opcVerdadera = view.findViewById(R.id.pregVerdadera);
        opcFalsa = view.findViewById(R.id.pregFalsa);

        switch (numPreguntaT) {
            case 1:
                //LAGNA DE GUATAVITA
                imagenPreg.setImageResource(R.drawable.lagunaguatavita);
                txtPregunta.setText("¿En la laguna de Guatavita navega la leyenda del Dorado?");
                break;

            case 2:
                //TODO CAMBIAR POR LAGUNA DE MARTOS
                imagenPreg.setImageResource(R.drawable.guasca);
                txtPregunta.setText("¿La laguna de Guasca también es conocida como laguna de Martos?");
                break;

            case 3:
                //LAGUNA DE UBAQUE
                imagenPreg.setImageResource(R.drawable.lagunaubaque);
                txtPregunta.setText("¿En la laguna de Ubaque se podían encontrar tunjos de oro y joyas con pedrería fina?");
                break;
        }
        onClick(view);
        starTimer();

        return view;
    }

    private void starTimer() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                sonidos.sonidoError();
                countDownTimer.cancel();
                checkPregunta(numPreguntaT, false);
                actualizarPreguntaT();
                Intent intent = new Intent(getActivity(), SiguienteLActivity.class);
                intent.putExtra("tipo", 4);
                intent.putExtra("validarAcierto", false);
                actualizarPreguntaT();
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
    }

    public void onClick(View view) {
        //TODO: INTENT PARA PASAR A LA VENTANA DE CORRECTO O INCORRECTO
        final Intent intent = new Intent(getActivity(), SiguienteLActivity.class);
        intent.putExtra("tipo", 4);

        opcVerdadera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numPreguntaT == 1) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaT, true);
                    actualizarPreguntaT();
                    opcVerdadera.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();

                } else if (numPreguntaT == 2) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaT, true);
                    actualizarPreguntaT();
                    opcVerdadera.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();

                } else if (numPreguntaT == 3) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaT, true);
                    actualizarPreguntaT();
                    opcVerdadera.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    checkPregunta(numPreguntaT, false);
                    actualizarPreguntaT();
                    opcVerdadera.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    intent.putExtra("validarAcierto", false);
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }

            }
        });

        opcFalsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sonidos.sonidoError();
                checkPregunta(numPreguntaT, false);
                actualizarPreguntaT();
                opcFalsa.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();

            }
        });


    }

    private void checkPregunta(int num, boolean check) {
        SharedPreferences.Editor editor = persistencia_lagunas.edit();
        if (num == 1 && check) {
            editor.putInt("checkP1", 2);
        } else if (num == 1 && !check) {
            editor.putInt("checkP1", 3);
        } else if (num == 2 && check) {
            editor.putInt("checkP3", 2);
        } else if (num == 2 && !check) {
            editor.putInt("checkP3", 3);
        } else if (num == 3 && check) {
            editor.putInt("checkP9", 2);
        } else if (num == 3 && !check) {
            editor.putInt("checkP9", 3);
        }
        editor.commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void actualizarPreguntaT() {
        //TODO: VARIABLES PARA CAMBIAR EL NUMERO DE PREGUNTA
        numPreguntaT = persistencia_lagunas.getInt("trueLagunas", 0) + 1;
        final SharedPreferences.Editor editor = persistencia_lagunas.edit();
        editor.putInt("trueLagunas", numPreguntaT);
        editor.commit();

    }

    //TODO: METODO PARA RESTAR VIDAS
    private void restarVidasFragment() {
        restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
        toolbar.restarVidas(restaVida);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();

    }
}

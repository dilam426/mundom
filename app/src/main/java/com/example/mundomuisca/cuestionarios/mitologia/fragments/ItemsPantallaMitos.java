package com.example.mundomuisca.cuestionarios.mitologia.fragments;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.mundomuisca.R;

public class ItemsPantallaMitos {

    View view;
    Context c;
    private String titulo, lectura, video;


    public ItemsPantallaMitos(String video, String titulo, String lectura){
        this.titulo = titulo;
        this.video = video;
        this.lectura = lectura;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }


    public String getVideo() {


        return video;
    }

    public void setVideo(String video) {

        this.video = video;
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }
}

package com.example.mundomuisca.cuestionarios.costumbres;

import androidx.appcompat.app.AppCompatActivity;
import com.example.mundomuisca.R;
import com.example.mundomuisca.menu.MenuActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SiguienteCActivity extends AppCompatActivity {

    private int tipo, numPregunta, counterC, numAciertosC;
    TextView tvCounter, tvMsgPregunta;
    SharedPreferences.Editor validadorC;
    private Button btnContinuar;
    private boolean validarAcierto;
    LinearLayout linearRecompensas, linearLogro;

    private SharedPreferences persistencia_costumbres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siguiente_c);

        tvCounter = findViewById(R.id.contador);
        tvMsgPregunta = findViewById(R.id.msgPregunta);
        btnContinuar = findViewById(R.id.btnContinuar);
        linearRecompensas = findViewById(R.id.linearRecompensas);
        linearLogro = findViewById(R.id.linearLogro);

        linearRecompensas.setVisibility(View.GONE);
        linearLogro.setVisibility(View.GONE);

        //TODO: VARIABLES PARA VALIDAR ACIERTO O ERROR DE LA PREGUNTA
        Intent intent = getIntent();
        tipo = intent.getIntExtra("tipo", 0);
        validarAcierto = intent.getBooleanExtra("validarAcierto", false);

        //TODO: VARIABLES PARA LLEVAR CONTEO DE ACIERTOS
        persistencia_costumbres = getSharedPreferences("PERSISTENCIA_COSTUMBRES", Context.MODE_PRIVATE);
        numAciertosC = persistencia_costumbres.getInt("aciertosC", 0);
        validadorC = persistencia_costumbres.edit();

        //TODO: VALIDAR SI LA RESPUESTA FUE CORRECTA O INCORRECTA
        if (validarAcierto) {
            tvMsgPregunta.setText("RESPUESTA CORRECTA!");
            numAciertosC = numAciertosC + 1;
            validadorC.putInt("aciertosC", numAciertosC);
            validadorC.commit();
        } else {
            tvMsgPregunta.setText("RESPUESTA INCORRECTA!");
        }

        //TODO: VARIABLES PARA LLEVAR EL NUMERO DE PREGUNTAS RESPONDIDAS
        counterC = persistencia_costumbres.getInt("counterPreguntasC", 1);
        tvCounter.setText(counterC + "/10");
        if (counterC == 10) {
            if (numAciertosC > 5) {
                SharedPreferences logroU = getSharedPreferences("logros", Context.MODE_PRIVATE);
                SharedPreferences.Editor upLogro = logroU.edit();
                upLogro.putBoolean("logroC", true);
                upLogro.commit();
                tvMsgPregunta.setText("Felicidades!!! " + numAciertosC + "/10");
                linearRecompensas.setVisibility(View.VISIBLE);
                linearLogro.setVisibility(View.VISIBLE);
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(true);
                    }
                });
            } else {
                tvMsgPregunta.setText("Intentalo de nuevo " + numAciertosC + "/10");
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(false);
                    }
                });
            }
        }

        SharedPreferences.Editor upCounterPU = persistencia_costumbres.edit();
        //TODO: CAMBIAR PARAMETRO PARA QUE CUENTE EL NUMERO DE PREGUNTAS
        counterC = counterC + 1;
        upCounterPU.putInt("counterPreguntasC", counterC);
        upCounterPU.apply();
    }

    public void siguientePregunta(View view){
        Intent siguiente;
        switch (tipo){
            case 1:
                numPregunta = persistencia_costumbres.getInt("multipleCostumbres", 1);
                if (numPregunta <= 3) {
                    siguiente = new Intent(this, QuizCActivity.class);
                    siguiente.putExtra("tipo", 2);
                    startActivity(siguiente);
                    this.finish();
                }else {
                    siguiente = new Intent(this, QuizCActivity.class);
                    siguiente.putExtra("tipo", 3);
                    startActivity(siguiente);
                    this.finish();
                }
                break;
            case 2:
                numPregunta = persistencia_costumbres.getInt("imagenCostumbres", 1);
                if (numPregunta <= 2) {
                    siguiente = new Intent(this, QuizCActivity.class);
                    siguiente.putExtra("tipo", 3);
                    startActivity(siguiente);
                    this.finish();
                }else {
                    siguiente = new Intent(this, QuizCActivity.class);
                    siguiente.putExtra("tipo", 4);
                    startActivity(siguiente);
                    this.finish();
                }
                break;
            case 3:
                siguiente = new Intent(this, QuizCActivity.class);
                siguiente.putExtra("tipo", 4);
                startActivity(siguiente);
                this.finish();
                break;
            case 4:
                numPregunta = persistencia_costumbres.getInt("completarCostumbres", 1);
                if (numPregunta <= 2) {
                    siguiente = new Intent(this, QuizCActivity.class);
                    siguiente.putExtra("tipo", 1);
                    startActivity(siguiente);
                    this.finish();
                }else {
                    siguiente = new Intent(this, QuizCActivity.class);
                    siguiente.putExtra("tipo", 2);
                    startActivity(siguiente);
                    this.finish();
                }
                break;
        }
    }

    private void finalizarQuizU(boolean completado){
        Intent intent = new Intent(this, MenuActivity.class);
        SharedPreferences recompensas = getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorRecompensas = recompensas.edit();
        if (completado) {
            int monedasActuales = recompensas.getInt("monedas", 100);
            int vidasActuales = recompensas.getInt("vidas", 10);
            editorRecompensas.putInt("monedas", monedasActuales + 100);
            editorRecompensas.putInt("vidas", vidasActuales + 3);
            editorRecompensas.putBoolean("recorrido", false);
            editorRecompensas.putBoolean("mitologia", false);
            editorRecompensas.putBoolean("costumbres", false);
            editorRecompensas.putBoolean("final", true);
        }

        SharedPreferences.Editor editor = persistencia_costumbres.edit();
        editor.putInt("completarCostumbres",1);
        editor.putInt("multipleCostumbres",1);
        editor.putInt("imagenCostumbres",1);
        editor.putInt("trueCostumbres",1);
        editor.putInt("counterPreguntasC", 1);
        validadorC.putInt("aciertosC", 0);
        validadorC.apply();
        editor.apply();
        editorRecompensas.apply();
        this.finish();
        startActivity(intent);
    }
}

package com.example.mundomuisca.cuestionarios.lagunas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.fragments.pop.up.DialogPopUpL;
import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.toolbar.Toolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class LagunasActivity extends AppCompatActivity implements View.OnClickListener, DialogPopUpL.OnFragmentInteractionListener {

    private TextView tvMonedas, tvVidas;
    private Toolbar toolbar;
    ImageView imgToolbar;
    Sonidos sonidos;

    private int numInfo;
    private int numQuestion;
    private FloatingActionButton info1,info2,info3,info4,info5;
    private FloatingActionButton question1,question2,question3,question4,question5, question6, question7, question8, question9, question10;
    private DialogPopUpL dialogPopUp;

    private SharedPreferences persistencia_lagunas;
    private SharedPreferences.Editor editorPreguntas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lagunas);

        persistencia_lagunas = getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
        numQuestion = persistencia_lagunas.getInt("numQuestion", 0);
        numInfo = persistencia_lagunas.getInt("numInfo", 1);

        iniLagunas();
        //Llamado al metodo cargar logros para que me traiga
        // la informacion almacenada en logros (Vidas - Mondedas)
        cargarLogros();
    }

    private void iniLagunas(){
        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);
        toolbar = new Toolbar(this,tvMonedas,tvVidas);
        toolbar.cargarLogros();
        info1 = findViewById(R.id.btnInfo1);
        info2 = findViewById(R.id.btnInfo2);
        info3 = findViewById(R.id.btnInfo3);
        info4 = findViewById(R.id.btnInfo4);
        info5 = findViewById(R.id.btnInfo5);
        question1 = findViewById(R.id.btnPregunta1);
        question2 = findViewById(R.id.btnPregunta2);
        question3 = findViewById(R.id.btnPregunta3);
        question4 = findViewById(R.id.btnPregunta4);
        question5 = findViewById(R.id.btnPregunta5);
        question6 = findViewById(R.id.btnpregunta6);
        question7 = findViewById(R.id.btnPregunta7);
        question8 = findViewById(R.id.btnPregunta8);
        question9 = findViewById(R.id.btnPregunta9);
        question10 = findViewById(R.id.btnPregunta10);
        dialogPopUp = new DialogPopUpL();
        imgToolbar = findViewById(R.id.logoMuisca);
        imgToolbar.setImageResource(R.drawable.back);
        sonidos = new Sonidos(this);

        //Escucha sobre los botones
        info1.setOnClickListener(this);
        info2.setOnClickListener(this);
        info3.setOnClickListener(this);
        info4.setOnClickListener(this);
        info5.setOnClickListener(this);
        question1.setOnClickListener(this);
        question2.setOnClickListener(this);
        question3.setOnClickListener(this);
        question4.setOnClickListener(this);
        question5.setOnClickListener(this);
        question6.setOnClickListener(this);
        question7.setOnClickListener(this);
        question8.setOnClickListener(this);
        question9.setOnClickListener(this);
        question10.setOnClickListener(this);
        imgToolbar.setOnClickListener(this);

        if (persistencia_lagunas.getInt("checkP1", 1) == 2){
            question1.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP1", 1) == 3){
            question1.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP2", 1) == 2){
            question2.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP2", 1) == 3){
            question2.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP3", 1) == 2){
            question3.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP3", 1) == 3){
            question3.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP4", 1) == 2){
            question4.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP4", 1) == 3){
            question4.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP5", 1) == 2){
            question5.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP5", 1) == 3){
            question5.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP6", 1) == 2){
            question6.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP6", 1) == 3){
            question6.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP7", 1) == 2){
            question7.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP7", 1) == 3){
            question7.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP8", 1) == 2){
            question8.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP8", 1) == 3){
            question8.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP9", 1) == 2){
            question9.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP9", 1) == 3){
            question9.setImageResource(R.drawable.incorrect);
        }

        if (persistencia_lagunas.getInt("checkP10", 1) == 2){
            question10.setImageResource(R.drawable.correct);
        }else if (persistencia_lagunas.getInt("checkP10", 1) == 3){
            question10.setImageResource(R.drawable.incorrect);
        }
    }

    private void cargarLogros(){

        SharedPreferences logros = getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        String modenas = "" + logros.getInt("monedas", 100);
        String vidas =  "" + logros.getInt("vidas", 10);

        tvMonedas.setText(modenas);
        tvVidas.setText(vidas);

    }

    @Override
    public void onClick(View v) {
        Bundle info = new Bundle();
        switch (v.getId()){
            case R.id.btnInfo1:
                if (numInfo == 1){
                info.putString("laguna","guatavita");
                dialogPopUp.setArguments(info);
                dialogPopUp.show(getSupportFragmentManager(), "DialogFragment");
                numQuestion = 1;
                }
                break;
            case R.id.btnPregunta1:
                if (numQuestion == 1){
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo",4);
                    actualizarPregunta(2);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnPregunta2:
                if (numQuestion == 2) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo",3);
                    actualizarInfo(2);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnInfo2:
                if (numInfo == 2) {
                    info.putString("tipoPopUp", "lagunas");
                    info.putString("laguna", "martos");
                    dialogPopUp.setArguments(info);
                    dialogPopUp.show(getSupportFragmentManager(), "DialogFragment");
                    numQuestion = 3;
                }
                break;
            case R.id.btnPregunta3:
                if (numQuestion == 3) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 4);
                    actualizarPregunta(4);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnPregunta4:
                if (numQuestion == 4) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 2);
                    actualizarInfo(3);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnInfo3:
                if (numInfo == 3) {
                    info.putString("tipoPopUp", "lagunas");
                    info.putString("laguna", "siecha");
                    dialogPopUp.setArguments(info);
                    dialogPopUp.show(getSupportFragmentManager(), "DialogFragment");
                    numQuestion = 5;

                }
                break;
            case R.id.btnPregunta5:
                if (numQuestion == 5) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 1);
                    actualizarPregunta(6);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnpregunta6:
                if (numQuestion == 6) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 2);
                    actualizarInfo(4);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnInfo4:
                if (numInfo == 4) {
                    info.putString("tipoPopUp", "lagunas");
                    info.putString("laguna", "teusaca");
                    dialogPopUp.setArguments(info);
                    dialogPopUp.show(getSupportFragmentManager(), "DialogFragment");
                    numQuestion = 7;
                }
                break;
            case R.id.btnPregunta7:
                if (numQuestion == 7) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 1);
                    actualizarPregunta(8);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnPregunta8:
                if (numQuestion == 8) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 2);
                    actualizarInfo(5);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnInfo5:
                if (numInfo == 5) {
                    info.putString("tipoPopUp", "lagunas");
                    info.putString("laguna", "ubaque");
                    dialogPopUp.setArguments(info);
                    dialogPopUp.show(getSupportFragmentManager(), "DialogFragment");
                    numQuestion = 9;
                }
                break;
            case R.id.btnPregunta9:
                if (numQuestion == 9) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 4);
                    actualizarPregunta(10);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.btnPregunta10:
                if (numQuestion == 10) {
                    Intent quizL = new Intent(this, QuizLActivity.class);
                    quizL.putExtra("tipo", 2);
                    actualizarPregunta(11);
                    startActivity(quizL);
                    this.finish();
                }
                break;
            case R.id.logoMuisca:
                sonidos.sonidoClic();
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }
    }

    private void actualizarInfo(int next){
        editorPreguntas = persistencia_lagunas.edit();
        editorPreguntas.putInt("numInfo",next);
        editorPreguntas.apply();

    }

    private void actualizarPregunta(int next){
        editorPreguntas = persistencia_lagunas.edit();
        editorPreguntas.putInt("numQuestion",next);
        editorPreguntas.apply();

    }

    //Método para cerrar la ventana Pop Up
    public void cerrarPopUp(View view) {
        dialogPopUp.dismiss();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

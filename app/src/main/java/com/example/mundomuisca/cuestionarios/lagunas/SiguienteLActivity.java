package com.example.mundomuisca.cuestionarios.lagunas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.R;

public class SiguienteLActivity extends AppCompatActivity {

    private int tipo, numPregunta, counterLagunas, numAciertosLagunas;
    TextView tvCounter, tvMsgPregunta;
    SharedPreferences persistencia_lagunas;
    SharedPreferences.Editor validadorL;
    private Button btnContinuar;
    private boolean validarAcierto;
    LinearLayout linearRecompensas, linearLogro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siguiente_l);

        tvCounter = findViewById(R.id.contador);
        tvMsgPregunta = findViewById(R.id.msgPregunta);
        btnContinuar = findViewById(R.id.btnContinuar);
        linearRecompensas = findViewById(R.id.linearRecompensas);
        linearLogro = findViewById(R.id.linearLogro);

        linearRecompensas.setVisibility(View.GONE);
        linearLogro.setVisibility(View.GONE);

        //TODO: VARIABLES PARA VALIDAR ACIERTO O ERROR DE LA PREGUNTA
        Intent intent = getIntent();
        validarAcierto = intent.getBooleanExtra("validarAcierto", false);

        //TODO: VARIABLES PARA LLEVAR CONTEO DE ACIERTOS
        persistencia_lagunas = getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
        numAciertosLagunas = persistencia_lagunas.getInt("aciertosL", 0);
        validadorL = persistencia_lagunas.edit();

        //TODO: VALIDAR SI LA RESPUESTA FUE CORRECTA O INCORRECTA
        if (validarAcierto) {
            tvMsgPregunta.setText("RESPUESTA CORRECTA!");
            numAciertosLagunas = numAciertosLagunas + 1;
            validadorL.putInt("aciertosL", numAciertosLagunas);
            validadorL.commit();
        } else {
            tvMsgPregunta.setText("RESPUESTA INCORRECTA!");
        }

        //TODO: VARIABLES PARA LLEVAR EL NUMERO DE PREGUNTAS RESPONDIDAS
        counterLagunas = persistencia_lagunas.getInt("counterPreguntasL", 1);
        tvCounter.setText(counterLagunas + "/10");
        if (counterLagunas >= 10) {
            if (numAciertosLagunas > 5) {
                SharedPreferences logroU = getSharedPreferences("logros", Context.MODE_PRIVATE);
                SharedPreferences.Editor upLogro = logroU.edit();
                upLogro.putBoolean("logroR", true);
                upLogro.commit();
                tvMsgPregunta.setText("Felicidades!!! " + numAciertosLagunas + "/10");
                linearRecompensas.setVisibility(View.VISIBLE);
                linearLogro.setVisibility(View.VISIBLE);
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(true);
                    }
                });
            } else {
                tvMsgPregunta.setText("Intentalo de nuevo " + numAciertosLagunas + "/10");
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(false);
                    }
                });
            }
        }

        SharedPreferences.Editor upCounterPU = persistencia_lagunas.edit();
        //TODO: CAMBIAR PARAMETRO PARA QUE CUENTE EL NUMERO DE PREGUNTAS
        counterLagunas = counterLagunas + 1;
        upCounterPU.putInt("counterPreguntasL", counterLagunas);
        upCounterPU.apply();
    }

    public void siguientePregunta(View view) {
        Intent siguiente = new Intent(this, LagunasActivity.class);
        startActivity(siguiente);
    }

    public void finalizarQuizU(boolean completado) {
        Intent intent = new Intent(this, MenuActivity.class);
        SharedPreferences recompensas = getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorRecompensas = recompensas.edit();
        if (completado) {
            int monedasActuales = recompensas.getInt("monedas", 100);
            int vidasActuales = recompensas.getInt("vidas", 10);
            editorRecompensas.putInt("monedas", monedasActuales + 100);
            editorRecompensas.putInt("vidas", vidasActuales + 3);
            editorRecompensas.putBoolean("mitologia", true);
            editorRecompensas.apply();

        }else {
            SharedPreferences persistenciaLagunas = getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
            SharedPreferences.Editor resetLagunas = persistenciaLagunas.edit();
            resetLagunas.putInt("completarLagunas", 1);
            resetLagunas.putInt("multipleLagunas", 1);
            resetLagunas.putInt("imagenLagunas", 1);
            resetLagunas.putInt("trueLagunas", 1);
            resetLagunas.putInt("counterPreguntasL", 1);
            resetLagunas.putInt("numQuestion", 0);
            resetLagunas.putInt("numInfo",1);
            resetLagunas.putInt("aciertosL", 0);
            resetLagunas.putInt("checkP1", 1);
            resetLagunas.putInt("checkP2", 1);
            resetLagunas.putInt("checkP3", 1);
            resetLagunas.putInt("checkP4", 1);
            resetLagunas.putInt("checkP5", 1);
            resetLagunas.putInt("checkP6", 1);
            resetLagunas.putInt("checkP7", 1);
            resetLagunas.putInt("checkP8", 1);
            resetLagunas.putInt("checkP9", 1);
            resetLagunas.putInt("checkP10", 1);
            resetLagunas.apply();
        }
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }
}
package com.example.mundomuisca.cuestionarios.mitologia.preguntas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.menu.MenuActivity;

public class QuizMActivity extends AppCompatActivity implements CompletarMFragment.OnFragmentInteractionListener, MultipleMFragment.OnFragmentInteractionListener, ImagenMFragment.OnFragmentInteractionListener, TrueFalseMFragment.OnFragmentInteractionListener {

    private TextView tvMonedas, tvVidas;
    CompletarMFragment completar;
    MultipleMFragment multiple;
    ImagenMFragment imagen;
    TrueFalseMFragment dosOpciones;
    private int tipo, numCompletar, numMultiple, numImage, numTrue;

    private SharedPreferences persistencia_mitos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_m);

        completar = new CompletarMFragment();
        multiple =  new MultipleMFragment();
        imagen = new ImagenMFragment();
        dosOpciones = new TrueFalseMFragment();

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);

        //Llamado al metodo cargar logros para que me traiga
        // la informacion almacenada en logros (Vidas - Mondedas)
        cargarLogros();

        Intent intent = getIntent();
        tipo = intent.getIntExtra("tipo", 0);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (Integer.parseInt(tvVidas.getText().toString()) > 0) {
            switch (tipo) {
                case 0:
                    persistencia_mitos = getSharedPreferences("PERSISTENCIA_MITOS", Context.MODE_PRIVATE);

                    numCompletar = persistencia_mitos.getInt("completarMitos", 1);
                    numMultiple = persistencia_mitos.getInt("multipleMitos", 1);
                    numImage = persistencia_mitos.getInt("imagenMitos", 1);
                    numTrue = persistencia_mitos.getInt("trueMitos", 1);
                    if (numCompletar <= 2) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, completar).commit();
                    } else if (numMultiple <= 3) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, multiple).commit();
                    } else if (numImage <= 2) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, imagen).commit();
                    } else if (numTrue <= 3) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, dosOpciones).commit();
                    } else {
                        Intent intnt = new Intent(this, SiguienteMActivity.class);
                        startActivity(intnt);
                    }
                    break;
                case 1:
                    transaction.replace(R.id.contenedorPU, completar).commit();
                    break;
                case 2:
                    transaction.replace(R.id.contenedorPU, multiple).commit();
                    break;
                case 3:
                    transaction.replace(R.id.contenedorPU, imagen).commit();
                    break;
                case 4:
                    transaction.replace(R.id.contenedorPU, dosOpciones).commit();
                    break;
            }
        }else {
            Intent newVidas = new Intent(this, MenuActivity.class);
            newVidas.putExtra("fragmentVidas", true);
            startActivity(newVidas);
            this.finish();
        }
    }

    //Metodo para cargar los datos al macenados en logros
    private void cargarLogros(){

        SharedPreferences logros = getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        String modenas = "" + logros.getInt("monedas", 100);
        String vidas =  "" + logros.getInt("vidas", 10);

        tvMonedas.setText(modenas);
        tvVidas.setText(vidas);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

package com.example.mundomuisca.cuestionarios.mitologia.preguntas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.R;
import com.example.mundomuisca.ScreenShot;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.toolbar.Toolbar;

import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MultipleMFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MultipleMFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MultipleMFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button optionA, optionB, optionC, optionD;

    private View view;
    private Toolbar toolbar;

    private Button ayuda50, ayudaAmigo;

    private int numPreguntaM = 0;
    TextView txtPregunta;

    TextView tvVidas, tvMonedas;
    int restaVida;

    private Boolean resta;

    private Sonidos sonidos;
    private ScreenShot screenShot;
    private TextView timer;
    private CountDownTimer countDownTimer;

    private SharedPreferences persistencia_mitos;

    private OnFragmentInteractionListener mListener;

    public MultipleMFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MultipleMFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MultipleMFragment newInstance(String param1, String param2) {
        MultipleMFragment fragment = new MultipleMFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_multiple_m, container, false);

        //Creacion de toolbar
        tvVidas = ((QuizMActivity) getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((QuizMActivity) getActivity()).findViewById(R.id.editTextMonedas);
        toolbar = new Toolbar(getContext(), tvMonedas, tvVidas);

        //Variable para validar el numero de pregunta
        persistencia_mitos = getActivity().getSharedPreferences("PERSISTENCIA_MITOS", Context.MODE_PRIVATE);
        numPreguntaM = persistencia_mitos.getInt("multipleMitos", 1);

        screenShot = new ScreenShot(getActivity(), getContext());
        sonidos = new Sonidos(getContext());

        timer = view.findViewById(R.id.timer);

        timer.setText(String.valueOf(30));

        ayuda50 = view.findViewById(R.id.ayuda50);
        ayudaAmigo = view.findViewById(R.id.ayudaAmigo);
        ayudaAmigo.setClickable(false);
        txtPregunta = view.findViewById(R.id.txtPregunta);
        optionA = view.findViewById(R.id.opcionA);
        optionB = view.findViewById(R.id.opcionB);
        optionC = view.findViewById(R.id.opcionC);
        optionD = view.findViewById(R.id.opcionD);

        //Validacion para agregar el contenido a la vista segun la pregunta
        switch (numPreguntaM) {
            case 1:
                txtPregunta.setText("¿Qué dios dió origen a los padres de la humanidad?");
                optionA.setText("Chiminigagua");
                optionB.setText("Goranchacha");
                optionC.setText("Bochica");
                optionD.setText("Chibchacum");
                break;
            case 2:
                txtPregunta.setText("¿Qué actividades aprendieron hacer Bachué y su acompañante?");
                optionA.setText("Nadar, cocinar, jugar");
                optionB.setText("Trabajar las piedras, manipular el fuego, recolectar y cazar sus alimentos");
                optionC.setText("Recolectar, tejer, cultivar");
                optionD.setText("Trabajar las piedras, nadar y tejer");
                break;
            case 3:
                txtPregunta.setText("¿Qué produjo el gobierno de Goranchacha al pueblo muisca?");
                optionA.setText("Hermandad, paz, amor");
                optionB.setText("Odio, esclavitud, avaricia ");
                optionC.setText("Esclavitud, instauró la magia negra, pervirtió a la gente");
                optionD.setText("Destrucción, hambre, pobreza");
                break;
        }
        onClick(view);
        starTimer();

        return view;
    }

    private void starTimer() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                sonidos.sonidoError();
                countDownTimer.cancel();
                actualizarPreguntaM();
                Intent intent = new Intent(getActivity(), SiguienteMActivity.class);
                intent.putExtra("tipo", 2);
                intent.putExtra("validarAcierto", false);
                actualizarPreguntaM();
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void onClick(View view) {

        //TODO: INTENT PARA PASAR A LA VENTANA DE CORRECTO O INCORRECTO
        final Intent intent = new Intent(getActivity(), SiguienteMActivity.class);
        intent.putExtra("tipo", 2);


        optionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 1) {
                    sonidos.sonidoCorrecto();
                    actualizarPreguntaM();
                    optionA.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    actualizarPreguntaM();
                    optionA.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        optionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 2) {
                    sonidos.sonidoCorrecto();
                    actualizarPreguntaM();
                    optionB.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    actualizarPreguntaM();
                    optionB.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }

            }

        });


        optionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 3) {
                    sonidos.sonidoCorrecto();
                    optionC.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    actualizarPreguntaM();

                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    actualizarPreguntaM();
                    intent.putExtra("validarAcierto", false);
                    optionC.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        optionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sonidos.sonidoError();
                actualizarPreguntaM();
                optionD.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        });

        ayuda50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoPista();
                    ayuda50.setClickable(false);
                    switch (numPreguntaM) {
                        case 1:
                            optionB.setVisibility(View.INVISIBLE);
                            optionD.setVisibility(View.INVISIBLE);
                            break;
                        case 2:
                            optionA.setVisibility(View.INVISIBLE);
                            optionC.setVisibility(View.INVISIBLE);
                            break;
                        case 3:
                            optionA.setVisibility(View.INVISIBLE);
                            optionD.setVisibility(View.INVISIBLE);
                            break;
                    }
                } else {
                    toastPersonalizado();
                }
            }
        });

        ayudaAmigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoScreen();
                    screenShot.ScreenShott(v);
                } else {
                    toastPersonalizado();
                }
            }
        });
    }

    private void actualizarPreguntaM() {
        //TODO: VARIABLES PARA CAMBIAR EL NUMERO DE PREGUNTA
        numPreguntaM = persistencia_mitos.getInt("multipleMitos", 0) + 1;
        final SharedPreferences.Editor editor = persistencia_mitos.edit();
        editor.putInt("multipleMitos", numPreguntaM);
        editor.commit();

    }

    //TODO: METODO PARA RESTAR VIDAS
    private void restarVidasFragment() {
        restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
        toolbar.restarVidas(restaVida);
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("No tienes monedas suficientes!");
        Toast toast = new Toast(getContext().getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }
}

package com.example.mundomuisca.cuestionarios.ubicacion;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.R;
import com.example.mundomuisca.ScreenShot;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.toolbar.Toolbar;


import java.io.IOException;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MultipleUFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private Button optionA, optionB, optionC, optionD;
    private String mParam1;
    private String mParam2;

    private View view;

    private Button ayuda50, ayudaAmigo;

    private int numPreguntaM = 0;
    TextView txtPregunta;

    TextView tvVidas, tvMonedas;
    Toolbar toolbar;
    int restaVida;

    private Sonidos sonidos;
    ScreenShot screenShot;
    private TextView timer;
    private CountDownTimer countDownTimer;

    private Boolean resta;

    private OnFragmentInteractionListener mListener;

    public MultipleUFragment() {
        // Required empty public constructor
    }

    public static MultipleUFragment newInstance(String param1, String param2) {
        MultipleUFragment fragment = new MultipleUFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_multiple_u, container, false);

        //Creacion de toolbar
        tvVidas = ((QuizUActivity) getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((QuizUActivity) getActivity()).findViewById(R.id.editTextMonedas);
        toolbar = new Toolbar(getContext(), tvMonedas, tvVidas);

        //Variable para validar el numero de pregunta
        SharedPreferences numPreguntaU = getActivity().getSharedPreferences("numPreguntaU", Context.MODE_PRIVATE);
        numPreguntaM = numPreguntaU.getInt("multiple", 0);

        screenShot = new ScreenShot(getActivity(), getContext());
        sonidos = new Sonidos(getContext());

        timer = view.findViewById(R.id.timer);

        timer.setText(String.valueOf(30));

        ayuda50 = view.findViewById(R.id.ayuda50);
        ayudaAmigo = view.findViewById(R.id.ayudaAmigo);
        ayudaAmigo.setClickable(false);
        txtPregunta = view.findViewById(R.id.txtPregunta);
        optionA = view.findViewById(R.id.opcionA);
        optionB = view.findViewById(R.id.opcionB);
        optionC = view.findViewById(R.id.opcionC);
        optionD = view.findViewById(R.id.opcionD);


        //Validacion para agregar el contenido a la vista segun la pregunta
        switch (numPreguntaM) {
            case 1:
                txtPregunta.setText("¿En que departamentos de Colombia se encontraba ubicada la cultura Muisca?");
                optionA.setText("Amazonas, Antioquia, Cundinamarca");
                optionB.setText("Boyacá, Arauca, Caldas");
                optionC.setText("Cundinamarca, Boyacá, Santander");
                optionD.setText("Huila, Magdalena, Santander");
                break;
            case 2:
                txtPregunta.setText("¿Cuáles fueron los grupos político-administrativos que gobernaron en el pueblo Muisca?");
                optionA.setText("Cacique, sacerdote");
                optionB.setText("Zipazgo, Zacazgo, tribu independiente");
                optionC.setText("Artesanos, comerciantes");
                optionD.setText("Ahawel(señorío), sahal(gobernador)");
                break;
            case 3:
                txtPregunta.setText("¿Desde que siglo los Muiscas habitaron Colombia?");
                optionA.setText("Siglo VI a. C.");
                optionB.setText("Siglo XX a. C.");
                optionC.setText("Siglo XIII a. C.");
                optionD.setText("Siglo IV a. C.");
                break;
        }
        onClick(view);
        starTimer();
        return view;
    }

    private void starTimer() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                sonidos.sonidoError();
                countDownTimer.cancel();
                actualizarPreguntaM();
                Intent intent = new Intent(getActivity(), SiguienteUActivity.class);
                intent.putExtra("tipo", 2);
                intent.putExtra("validarAcierto", false);
                actualizarPreguntaM();
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void onClick(View view) {

        //TODO: INTENT PARA PASAR A LA VENTANA DE CORRECTO O INCORRECTO
        final Intent intent = new Intent(getActivity(), SiguienteUActivity.class);
        intent.putExtra("tipo", 2);


        optionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 3) {
                    sonidos.sonidoCorrecto();
                    actualizarPreguntaM();
                    optionA.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    actualizarPreguntaM();
                    optionA.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        optionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 2) {
                    sonidos.sonidoCorrecto();
                    actualizarPreguntaM();
                    optionB.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    actualizarPreguntaM();
                    optionB.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }

            }

        });


        optionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 1) {
                    sonidos.sonidoCorrecto();
                    optionC.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    actualizarPreguntaM();

                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();

                } else {
                    sonidos.sonidoError();
                    actualizarPreguntaM();
                    intent.putExtra("validarAcierto", false);
                    optionC.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        optionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sonidos.sonidoError();
                actualizarPreguntaM();
                optionD.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        });

        ayuda50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoPista();
                    ayuda50.setClickable(false);
                    switch (numPreguntaM) {
                        case 1:
                            optionA.setVisibility(View.INVISIBLE);
                            optionD.setVisibility(View.INVISIBLE);
                            break;
                        case 2:
                            optionA.setVisibility(View.INVISIBLE);
                            optionC.setVisibility(View.INVISIBLE);
                            break;
                        case 3:
                            optionB.setVisibility(View.INVISIBLE);
                            optionD.setVisibility(View.INVISIBLE);
                            break;
                    }
                } else {
                    toastPersonalizado();
                }
            }
        });

        ayudaAmigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoScreen();
                    screenShot.ScreenShott(v);
                }else {
                    toastPersonalizado();
                }
            }
        });
    }

    private void actualizarPreguntaM() {
        //TODO: VARIABLES PARA CAMBIAR EL NUMERO DE PREGUNTA
        SharedPreferences numPreguntaU = getActivity().getSharedPreferences("numPreguntaU", Context.MODE_PRIVATE);
        numPreguntaM = numPreguntaU.getInt("multiple", 0) + 1;
        final SharedPreferences.Editor editor = numPreguntaU.edit();
        editor.putInt("multiple", numPreguntaM);
        editor.commit();

    }

    //TODO: METODO PARA RESTAR VIDAS
    private void restarVidasFragment() {
        restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
        toolbar.restarVidas(restaVida);
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("No tienes monedas suficientes!");
        Toast toast = new Toast(getContext().getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }
}

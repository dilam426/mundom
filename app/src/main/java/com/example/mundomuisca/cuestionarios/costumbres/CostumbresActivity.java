package com.example.mundomuisca.cuestionarios.costumbres;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.fragments.pop.up.DialogPopUpC;
import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.toolbar.Toolbar;

public class CostumbresActivity extends AppCompatActivity implements View.OnClickListener, DialogPopUpC.OnFragmentInteractionListener {

    private TextView tvMonedas, tvVidas;
    private Toolbar toolbar;
    private Button agricultura, aspecto, orfebreria, vestimenta;
    private DialogPopUpC dialogPopUpC;
    private Button btnQuiz;
    private Boolean c1=false, c2=false, c3=false, c4=false;
    ImageView imgToolbar;
    Sonidos sonidos;


    //Variables para el botón de incio del quiz y para el botón de cerrar en la alerta
    public Button closeAnuncio;

    //Layouts contendeor de la alerta y contenedor para el color de fondo oscuro
    LinearLayout contenidoAnuncio, overBox;

    //Logo de la alerta
    ImageView advertencia;

    //Animación para la alerta
    Animation fromsmall, fromnothing, fromAdverImg, togo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_costumbres);

        iniElementos();
        toolbar = new Toolbar(this, tvMonedas, tvVidas);
        toolbar.cargarLogros();

    }

    //Inicialización de los elementos de las interfaces
    public void iniElementos() {
        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);
        agricultura = findViewById(R.id.btnAgricultura);
        aspecto = findViewById(R.id.btnAspecto);
        orfebreria = findViewById(R.id.btnorfebreria);
        vestimenta = findViewById(R.id.btnVestimenta);
        btnQuiz = findViewById(R.id.btnQuizCultura);
        dialogPopUpC = new DialogPopUpC();
        closeAnuncio = findViewById(R.id.closeAviso);
        contenidoAnuncio = findViewById(R.id.contenidoBox);
        overBox = findViewById(R.id.overbox);
        advertencia = findViewById(R.id.adverImg);
        fromsmall = AnimationUtils.loadAnimation(this, R.anim.fromsmall);
        fromnothing = AnimationUtils.loadAnimation(this, R.anim.fromnothing);
        fromAdverImg = AnimationUtils.loadAnimation(this, R.anim.from_adver_img);
        togo = AnimationUtils.loadAnimation(this, R.anim.togo);
        imgToolbar = findViewById(R.id.logoMuisca);
        imgToolbar.setImageResource(R.drawable.back);
        sonidos = new Sonidos(this);


        //Escucha sobre los botones
        agricultura.setOnClickListener(this);
        aspecto.setOnClickListener(this);
        orfebreria.setOnClickListener(this);
        vestimenta.setOnClickListener(this);
        btnQuiz.setOnClickListener(this);
        closeAnuncio.setOnClickListener(this);
        imgToolbar.setOnClickListener(this);

        //Propiedad para que no sean visibles los contenedores
        contenidoAnuncio.setAlpha(0f);
        overBox.setAlpha(0f);
        advertencia.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {

        Bundle info = new Bundle();

        switch (v.getId()) {
            case R.id.btnAgricultura:
                info.putString("costumbre", "agricultura");
                dialogPopUpC.setArguments(info);
                dialogPopUpC.show(getSupportFragmentManager(), "DialogFRagment");
                c1 = true;
                break;
            case R.id.btnAspecto:
                info.putString("costumbre", "aspecto");
                dialogPopUpC.setArguments(info);
                dialogPopUpC.show(getSupportFragmentManager(), "DialogFragment");
                c2 = true;
                break;
            case R.id.btnorfebreria:
                info.putString("costumbre", "orfebreria");
                dialogPopUpC.setArguments(info);
                dialogPopUpC.show(getSupportFragmentManager(), "DialogFragment");
                c3 = true;
                break;
            case R.id.btnVestimenta:
                info.putString("costumbre", "vestimenta");
                dialogPopUpC.setArguments(info);
                dialogPopUpC.show(getSupportFragmentManager(), "DialogFRgament");
                c4 = true;
                break;
            case R.id.btnQuizCultura:
                iniciarQuiz();
                break;
            case R.id.closeAviso:
                cerrarAnuncio();
                break;
            case R.id.logoMuisca:
                sonidos.sonidoClic();
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void iniciarQuiz() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        boolean ingreso = preferences.getBoolean("IngresoC", false);
        if (Integer.parseInt(tvVidas.getText().toString()) <= 0) {
             toastPersonalizado();
        }else if (c1 && c2 && c3 && c4 || ingreso) {
            SharedPreferences.Editor quiz = preferences.edit();
            quiz.putBoolean("IngresoC", true);
            quiz.apply();
            Intent intent = new Intent(this, QuizCActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            advertencia.setVisibility(View.VISIBLE);
            advertencia.startAnimation(fromAdverImg);
            overBox.setAlpha(1f);
            overBox.startAnimation(fromnothing);
            contenidoAnuncio.setAlpha(1f);
            contenidoAnuncio.startAnimation(fromsmall);
        }
    }

    //Método para cerrar el anuncio que valida el ingreso al quiz
    public void cerrarAnuncio(){
        overBox.startAnimation(togo);
        advertencia.startAnimation(togo);
        contenidoAnuncio.startAnimation(togo);
        advertencia.setVisibility(View.GONE);
        ViewCompat.animate(contenidoAnuncio).setStartDelay(1000).alpha(0).start();
        ViewCompat.animate(overBox).setStartDelay(1000).alpha(0).start();
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("No tienes vidas!");
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }
}

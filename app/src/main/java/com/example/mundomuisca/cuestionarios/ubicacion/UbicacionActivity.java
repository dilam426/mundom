package com.example.mundomuisca.cuestionarios.ubicacion;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.toolbar.Toolbar;
import com.example.mundomuisca.fragments.pop.up.DialogPopUp;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class UbicacionActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "UbicacionActivity";

    private TextView tvMonedas, tvVidas;
    Toolbar toolbar;
    ImageView imgToolbar;
    Sonidos sonidos;

    //Variables de cada boton flotante
    public FloatingActionButton zoomIn, btnUbicacion, btnBoyaca, btnSantander, btnCundi;

    //Variable para crear animación al presionar el boton de ubicación
    private float translationY = 100f;

    //Variable para validar si el menu esta abierto o no
    private boolean isMenuOpen = false;

    //Variables para el botón de incio del quiz y para el botón de cerrar en la alerta
    public Button quiz, closeAnuncio;

    //Layouts contendeor de la alerta y contenedor para el color de fondo oscuro
    LinearLayout contenidoAnuncio, overBox;

    //Logo de la alerta
    ImageView advertencia;

    //Animación para la alerta
    Animation fromsmall, fromnothing, fromAdverImg, togo;
    private int estadoBtnQuiz = 1;

    //Variables para validar que se ingresen a los tres departamentos antes de realizar el quiz
    private boolean ingresoBoyaca = false, ingresoSantander = false, ingresoCundinamarca = false;

    //Instancia de los framents de cada departamento
    DialogPopUp dialogFragment;
    Dialog myDialog;
    OvershootInterpolator interpolator = new OvershootInterpolator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacion);

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);
        toolbar = new Toolbar(this, tvMonedas, tvVidas);
        toolbar.cargarLogros();
        imgToolbar = findViewById(R.id.logoMuisca);
        imgToolbar.setImageResource(R.drawable.back);
        sonidos = new Sonidos(this);


        zoomIn = findViewById(R.id.floatBtn);
        myDialog = new Dialog(this);

        zoomInImg();
        iniMenuUbicacion();
        animarBtnQuiz();
    }

    //Metodo que crea un alertDialog para ampliar la imagen del mapa Muisca
    private void zoomInImg() {
        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(UbicacionActivity.this);
                View view = getLayoutInflater().inflate(R.layout.alert_zoom, null);
                PhotoView  photoView = view.findViewById(R.id.imagePhotoView);
                photoView.setImageResource(R.drawable.mapamuisca1);
                builder.setView(view);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    //Método para inicializar las variables de los botones
    private void iniMenuUbicacion() {
        btnUbicacion = findViewById(R.id.floatUb);
        btnBoyaca = findViewById(R.id.floatBoyaca);
        btnSantander = findViewById(R.id.floatSantander);
        btnCundi = findViewById(R.id.floatCundi);
        quiz = findViewById(R.id.quizBtn);
        dialogFragment = new DialogPopUp();
        closeAnuncio = findViewById(R.id.closeAviso);
        contenidoAnuncio = findViewById(R.id.contenidoBox);
        overBox = findViewById(R.id.overbox);
        advertencia = findViewById(R.id.adverImg);
        fromsmall = AnimationUtils.loadAnimation(this, R.anim.fromsmall);
        fromnothing = AnimationUtils.loadAnimation(this, R.anim.fromnothing);
        fromAdverImg = AnimationUtils.loadAnimation(this, R.anim.from_adver_img);
        togo = AnimationUtils.loadAnimation(this, R.anim.togo);

        //Propiedad para que los botones no sean visibles
        btnSantander.setAlpha(0f);
        btnBoyaca.setAlpha(0f);
        btnCundi.setAlpha(0f);
        contenidoAnuncio.setAlpha(0f);
        overBox.setAlpha(0f);
        advertencia.setVisibility(View.GONE);

        //Transicion que tienen los botones antes de aparecer
        btnSantander.setTranslationY(translationY);
        btnBoyaca.setTranslationY(translationY);
        btnCundi.setTranslationY(translationY);

        //Escucha sobre los botones
        btnUbicacion.setOnClickListener(this);
        btnSantander.setOnClickListener(this);
        btnBoyaca.setOnClickListener(this);
        btnCundi.setOnClickListener(this);
        quiz.setOnClickListener(this);
        closeAnuncio.setOnClickListener(this);
        imgToolbar.setOnClickListener(this);

    }

    //Método para abrir el menu
    private void openMenu() {
        isMenuOpen = !isMenuOpen;

        //Animación del boton cuando es presionado
        btnUbicacion.animate().setInterpolator(interpolator).rotation(45f).setDuration(300).start();
        //Amimación cuando aparecen los botones
        btnSantander.animate().translationY(0f).alpha(1f).setInterpolator(interpolator).setDuration(300).start();
        btnBoyaca.animate().translationY(0f).alpha(1f).setInterpolator(interpolator).setDuration(300).start();
        btnCundi.animate().translationY(0f).alpha(1f).setInterpolator(interpolator).setDuration(300).start();

    }

    //Metodo para cerrar menu
    private void closeMenu() {
        isMenuOpen = !isMenuOpen;

        btnUbicacion.animate().setInterpolator(interpolator).rotation(0f).setDuration(300).start();
        //Animacion para desaparecer los botones
        btnSantander.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator).setDuration(300).start();
        btnBoyaca.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator).setDuration(300).start();
        btnCundi.animate().translationY(translationY).alpha(0f).setInterpolator(interpolator).setDuration(300).start();
    }

    //Métodos para que se muestre los DialogFRagment sobre información de cada departamento
    private void handleBtnSantander() {
        //Toast.makeText(this, "Santander", Toast.LENGTH_SHORT).show()
        //dialogFragment.show(getSupportFragmentManager(), "DialogFragment");
        Bundle info = new Bundle();
        info.putString("tipoPopUp", "ubicacion");
        info.putString("departamento", "santander");
        dialogFragment.setArguments(info);
        dialogFragment.show(getSupportFragmentManager(), "DialogFragment");
        ingresoSantander = true;

    }

    private void handleBtnBoyaca() {
        //Toast.makeText(this, "Boyacá", Toast.LENGTH_SHORT).show();
        Bundle info = new Bundle();
        info.putString("tipoPopUp", "ubicacion");
        info.putString("departamento", "boyaca");
        dialogFragment.setArguments(info);
        dialogFragment.show(getSupportFragmentManager(), "DialogFragment");
        ingresoBoyaca = true;

    }

    private void handleBtnCundi() {
        Bundle info = new Bundle();
        info.putString("tipoPopUp", "ubicacion");
        info.putString("departamento", "cundinamarca");
        dialogFragment.setArguments(info);
        dialogFragment.show(getSupportFragmentManager(), "DialogFragment");
        ingresoCundinamarca = true;

    }

    //Método para validar que boton fue presionado
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatUb:
                Log.i(TAG, "onClick: float Ub");
                if (isMenuOpen) {
                    closeMenu();
                } else {
                    openMenu();
                }
                break;
            case R.id.floatBoyaca:
                Log.i(TAG, "onClick: float Boyaca");
                handleBtnBoyaca();
                if (isMenuOpen) {
                    closeMenu();
                } else {
                    openMenu();
                }
                break;
            case R.id.floatSantander:
                Log.i(TAG, "onClick: float Santander");
                handleBtnSantander();
                if (isMenuOpen) {
                    closeMenu();
                } else {
                    openMenu();
                }
                break;
            case R.id.floatCundi:
                Log.i(TAG, "onClick: float Cundi");
                handleBtnCundi();
                if (isMenuOpen) {
                    closeMenu();
                } else {
                    openMenu();
                }
                break;

            //Metodo para validar el boton de quiz
            case R.id.quizBtn:
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("logrosO", Context.MODE_PRIVATE);
                boolean ingreso = preferences.getBoolean("Ingreso", false);
                if (Integer.parseInt(tvVidas.getText().toString()) <= 0) {
                    toastPersonalizado();
                } else if (ingresoSantander && ingresoBoyaca && ingresoCundinamarca || ingreso) {
                    SharedPreferences.Editor quiz = preferences.edit();
                    quiz.putBoolean("Ingreso", true);
                    quiz.apply();
                    Intent quizU = new Intent(UbicacionActivity.this, QuizUActivity.class);
                    startActivity(quizU);
                    UbicacionActivity.this.finish();
                } else {
                    advertencia.setVisibility(View.VISIBLE);
                    advertencia.startAnimation(fromAdverImg);
                    overBox.setAlpha(1f);
                    overBox.startAnimation(fromnothing);
                    contenidoAnuncio.setAlpha(1f);
                    contenidoAnuncio.startAnimation(fromsmall);
                }
                break;

            case R.id.closeAviso:

                overBox.startAnimation(togo);
                advertencia.startAnimation(togo);
                contenidoAnuncio.startAnimation(togo);
                advertencia.setVisibility(View.GONE);
                ViewCompat.animate(contenidoAnuncio).setStartDelay(1000).alpha(0).start();
                ViewCompat.animate(overBox).setStartDelay(1000).alpha(0).start();
                break;

            case R.id.logoMuisca:
                sonidos.sonidoClic();
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);
                this.finish();
                break;

        }
    }

    //Método para colocar animación al botón del quiz
    public void animarBtnQuiz() {
        Thread t = new Thread() {
            @Override
            public void run() {
                while (!isInterrupted()) {
                    try {
                        Thread.sleep(1000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (estadoBtnQuiz == 1) {
                                    quiz.animate().setInterpolator(interpolator).rotation(40f).setDuration(300).start();
                                    estadoBtnQuiz = 0;
                                } else {
                                    quiz.animate().setInterpolator(interpolator).rotation(0f).setDuration(300).start();
                                    estadoBtnQuiz = 1;
                                }
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        t.start();
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("No tienes vidas!");
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    //Método para cerrar la ventana Pop Up
    public void cerrarPopUp(View view) {
        dialogFragment.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
        toolbar.cargarLogros();
    }
}


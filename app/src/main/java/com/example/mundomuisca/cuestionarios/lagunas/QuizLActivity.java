package com.example.mundomuisca.cuestionarios.lagunas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.R;

public class QuizLActivity extends AppCompatActivity implements CompletarLFragment.OnFragmentInteractionListener, MultipleLFragment.OnFragmentInteractionListener, ImagenLFragment.OnFragmentInteractionListener, TrueFalseLFragment.OnFragmentInteractionListener{

    private TextView tvMonedas, tvVidas;
    private CompletarLFragment completar;
    private MultipleLFragment multiple;
    private ImagenLFragment imagen;
    private TrueFalseLFragment dosOpciones;
    private int tipo, numCompletar, numMultiple, numImage, numTrue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_l);

        completar = new CompletarLFragment();
        multiple =  new MultipleLFragment();
        imagen = new ImagenLFragment();
        dosOpciones = new TrueFalseLFragment();

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);

        //Llamado al metodo cargar logros para que me traiga
        // la informacion almacenada en logros (Vidas - Mondedas)
        cargarLogros();

        Intent intent = getIntent();
        tipo = intent.getIntExtra("tipo", 0);

        if (Integer.parseInt(tvVidas.getText().toString()) > 0) {
            switch (tipo) {
                case 1:
                    getSupportFragmentManager().beginTransaction().add(R.id.contenedorL, completar).commit();
                    break;
                case 2:
                    getSupportFragmentManager().beginTransaction().add(R.id.contenedorL, multiple).commit();
                    break;
                case 3:
                    getSupportFragmentManager().beginTransaction().add(R.id.contenedorL, imagen).commit();
                    break;
                case 4:
                    getSupportFragmentManager().beginTransaction().add(R.id.contenedorL, dosOpciones).commit();
                    break;
            }
        }else {
            Intent newVidas = new Intent(this, MenuActivity.class);
            newVidas.putExtra("fragmentVidas", true);
            startActivity(newVidas);
            QuizLActivity.this.finish();
        }
    }

    //Metodo para cargar los datos al macenados en logros
    private void cargarLogros(){

        SharedPreferences logros = getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        String modenas = "" + logros.getInt("monedas", 100);
        String vidas =  "" + logros.getInt("vidas", 10);

        tvMonedas.setText(modenas);
        tvVidas.setText(vidas);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

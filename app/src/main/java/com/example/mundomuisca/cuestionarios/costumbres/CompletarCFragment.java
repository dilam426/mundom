package com.example.mundomuisca.cuestionarios.costumbres;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.toolbar.Toolbar;

import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CompletarCFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CompletarCFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompletarCFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private  int numPreguntaC = 0;

    private int presCounter = 0;
    private int maxPresCounter;
    private String[]keys;
    private String textAnswer;
    private TextView txtPregunta, timer;
    private EditText editTxtPalabra;
    private LinearLayout linearLayout, linear2;
    private CountDownTimer countDownTimer;
    int contador;

    private Sonidos sonidos;

    TextView tvVidas, tvMonedas;
    private Toolbar toolbar;
    int restaVida;

    private SharedPreferences persistencia_costumbres;

    private OnFragmentInteractionListener mListener;

    public CompletarCFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CompletarCFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CompletarCFragment newInstance(String param1, String param2) {
        CompletarCFragment fragment = new CompletarCFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completar_c, container, false);

        //Creacion de toolbar
        tvVidas = ((QuizCActivity)getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((QuizCActivity)getActivity()).findViewById(R.id.editTextMonedas);
        toolbar =  new Toolbar(getContext(), tvMonedas,tvVidas);

        sonidos = new Sonidos(getContext());

        //Variable para validar cual es el numero de la pregunta
        persistencia_costumbres = getActivity().getSharedPreferences("PERSISTENCIA_COSTUMBRES", Context.MODE_PRIVATE);
        numPreguntaC = persistencia_costumbres.getInt("completarCostumbres", 1);

        txtPregunta = view.findViewById(R.id.txtPregunta);
        timer = view.findViewById(R.id.timer);

        timer.setText(String.valueOf(30));

        if (numPreguntaC == 1) {
            txtPregunta.setText("¿Uno de los animales que conformaba su dieta alimenticia?");
            keys = new String[]{"P", "E", "S", "C", "A", "D", "O"};
            textAnswer = "PESCADO";
            maxPresCounter = keys.length;
            numPreguntaC = numPreguntaC + 1;
        } else if (numPreguntaC == 2) {
            txtPregunta.setText("¿Qué producto cultivaban los Muiscas?");
            keys = new String[]{"M", "A", "I", "Z"};
            textAnswer = "MAIZ";
            maxPresCounter = keys.length;
            numPreguntaC = numPreguntaC + 1;
        }

        editTxtPalabra = view.findViewById(R.id.editTxtPalabra);
        linearLayout = view.findViewById(R.id.linearLetras);
        linear2 = view.findViewById(R.id.linearPrueba);

        keys = shuffleArray(keys);
        for (String key : keys) {
            if (contador < 5) {
                addView((LinearLayout) view.findViewById(R.id.linearLetras), key, ((EditText) view.findViewById(R.id.editTxtPalabra)));
            } else {
                addView((LinearLayout) view.findViewById(R.id.linearPrueba), key, ((EditText) view.findViewById(R.id.editTxtPalabra)));
            }
            contador++;
        }
        starTimer();

        return view;
    }

    private void starTimer(){
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(String.valueOf(millisUntilFinished/1000));
            }

            @Override
            public void onFinish() {
                sonidos.sonidoError();
                countDownTimer.cancel();
                SharedPreferences.Editor editor = persistencia_costumbres.edit();
                editor.putInt("completarCostumbres", numPreguntaC);
                editor.commit();
                Intent intent = new Intent(getActivity(), SiguienteCActivity.class);
                intent.putExtra("tipo", 1);
                intent.putExtra("validarAcierto", false);
                restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
                toolbar.restarVidas(restaVida);
                startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
    }

    private String[] shuffleArray(String ar[]){
        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--){
            int index = rnd.nextInt(i + 1);
            String a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }

        return ar;
    }

    private void addView(LinearLayout viewParent, final String text, final EditText editText) {

        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        linearLayoutParams.rightMargin = 5;
        final TextView textView = new TextView(getContext());
        textView.setLayoutParams(linearLayoutParams);
        textView.setBackground(getContext().getResources().getDrawable(R.drawable.rectangulo_letra));
        textView.setTextColor(getContext().getResources().getColor(R.color.colorBlanc));
        textView.setGravity(Gravity.CENTER);
        textView.setText(text);
        textView.setClickable(true);
        textView.setFocusable(true);
        textView.setTextSize(32);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(presCounter <= maxPresCounter){
                    if(presCounter >= 0){
                        //editText.setText("");
                        editText.setText(editText.getText().toString() + text);
                        // textView.startAnimation(bibsmallforth);
                        textView.animate().alpha(0).setDuration(300);
                        presCounter++;

                        if (presCounter == maxPresCounter){
                            doValidate();
                        }
                    }
                }
            }
        });

        viewParent.addView(textView);



    }

    private void doValidate(){

        //TODO: VARIABLES PARA CAMBIAR EL NUMERO DE PREGUNTA
        SharedPreferences.Editor editor = persistencia_costumbres.edit();

        presCounter = 0;

        //TODO: INTENT PARA IR A LA VISTA DE PREGUNTA SIGUIENTE
        Intent intent = new Intent(getActivity(), SiguienteCActivity.class);
        intent.putExtra("tipo", 1);


        if(editTxtPalabra.getText().toString().equals(textAnswer)){
            sonidos.sonidoCorrecto();
            editor.putInt("completarCostumbres", numPreguntaC);
            editor.commit();
            intent.putExtra("validarAcierto", true);
            startActivity(intent);
            getActivity().finish();
        }else {
            sonidos.sonidoError();
            editor.putInt("completarCostumbres", numPreguntaC);
            editor.commit();
            intent.putExtra("validarAcierto", false);

            restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
            toolbar.restarVidas(restaVida);

            getActivity().finish();
            startActivity(intent);
            editTxtPalabra.setText("");
        }

        keys = shuffleArray(keys);
        linearLayout.removeAllViews();
        linear2.removeAllViews();

        for(String key: keys){
            addView(linearLayout, key, editTxtPalabra);
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

package com.example.mundomuisca.cuestionarios.lagunas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.R;
import com.example.mundomuisca.ScreenShot;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.toolbar.Toolbar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ImagenLFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ImagenLFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImagenLFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    SharedPreferences persistencia_imagen;

    private Button ayuda50, ayudaAmigo;
    private ScreenShot screenShot;

    private View view;

    private ImageView opcionA, opcionB, opcionC, opcionD;
    private int numPreguntaI;
    private TextView txtPregunta, timer;
    private CountDownTimer countDownTimer;

    TextView tvVidas, tvMonedas;
    Toolbar toolbar;
    int restaVida;

    private Boolean resta;

    private Sonidos sonidos;

    private OnFragmentInteractionListener mListener;

    public ImagenLFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ImagenLFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImagenLFragment newInstance(String param1, String param2) {
        ImagenLFragment fragment = new ImagenLFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_imagen_l, container, false);

        //Creacion de toolbar
        tvVidas = ((QuizLActivity) getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((QuizLActivity) getActivity()).findViewById(R.id.editTextMonedas);
        toolbar = new Toolbar(getContext(), tvMonedas, tvVidas);

        screenShot = new ScreenShot(getActivity(), getContext());
        sonidos = new Sonidos(getContext());

        //Variable para validar el numero de pregunta
        persistencia_imagen = getActivity().getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
        numPreguntaI = persistencia_imagen.getInt("imagenLagunas", 1);

        timer = view.findViewById(R.id.timer);

        timer.setText(String.valueOf(30));

        ayuda50 = view.findViewById(R.id.ayuda50);
        ayudaAmigo = view.findViewById(R.id.ayudaAmigo);
        ayudaAmigo.setClickable(false);
        txtPregunta = view.findViewById(R.id.txtPregunta);
        opcionA = view.findViewById(R.id.opcionAIndio);
        opcionB = view.findViewById(R.id.opcionBIndio);
        opcionC = view.findViewById(R.id.opcionCIndio);
        opcionD = view.findViewById(R.id.opcionDIndio);

        if (numPreguntaI == 1) {
            //LAGUNA DE GUATAVITA
            txtPregunta.setText("¿Cual de estas es la laguna de Guatavita?");
            opcionA.setImageResource(R.drawable.lagunaubaque);
            opcionB.setImageResource(R.drawable.lagunaubaque);
            opcionC.setImageResource(R.drawable.lagunasiecha);
            opcionD.setImageResource(R.drawable.lagunaguatavita);
        }
        onClick(view);
        starTimer();

        return view;
    }

    private void starTimer() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                sonidos.sonidoError();
                countDownTimer.cancel();
                checkPregunta(numPreguntaI, false);
                actualizarPreguntaI();
                Intent intent = new Intent(getActivity(), SiguienteLActivity.class);
                intent.putExtra("tipo", 3);
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void onClick(View view) {

        //TODO: INTENT PARA PASAR A LA VENTANA DE CORRECTO O INCORRECTO
        final Intent intent = new Intent(getActivity(), SiguienteLActivity.class);
        intent.putExtra("tipo", 3);

        opcionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sonidos.sonidoError();
                checkPregunta(numPreguntaI, false);
                actualizarPreguntaI();
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        });

        opcionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sonidos.sonidoError();
                checkPregunta(numPreguntaI, false);
                actualizarPreguntaI();
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        });

        opcionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sonidos.sonidoError();
                checkPregunta(numPreguntaI, false);
                actualizarPreguntaI();
                intent.putExtra("validarAcierto", false);
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        });

        opcionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numPreguntaI == 1) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaI, true);
                    actualizarPreguntaI();
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    checkPregunta(numPreguntaI, false);
                    actualizarPreguntaI();
                    intent.putExtra("validarAcierto", false);
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        ayuda50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoPista();
                    ayuda50.setClickable(false);
                    opcionB.setVisibility(View.INVISIBLE);
                    opcionC.setVisibility(View.INVISIBLE);
                } else {
                    toastPersonalizado();
                }
            }
        });
        ayudaAmigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoScreen();
                    screenShot.ScreenShott(v);
                }else {
                    toastPersonalizado();
                }
            }
        });
    }

    private void actualizarPreguntaI() {
        //TODO: VARIABLES PARA CAMBIAR EL NUMERO DE PREGUNTA
        numPreguntaI = persistencia_imagen.getInt("imagenLagunas", 0) + 1;
        final SharedPreferences.Editor editor = persistencia_imagen.edit();
        editor.putInt("imagenLagunas", numPreguntaI);
        editor.commit();

    }

    //TODO: METODO PARA RESTAR VIDAS
    private void restarVidasFragment() {
        restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
        toolbar.restarVidas(restaVida);
    }

    private void checkPregunta(int num, boolean check) {
        SharedPreferences.Editor editor = persistencia_imagen.edit();
        if (num == 1 && check) {
            editor.putInt("checkP2", 2);
        } else if (num == 1 && !check) {
            editor.putInt("checkP2", 3);
        }
        editor.commit();
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("No tienes monedas suficientes!");
        Toast toast = new Toast(getContext().getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }
}

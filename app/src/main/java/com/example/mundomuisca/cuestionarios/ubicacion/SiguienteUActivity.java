package com.example.mundomuisca.cuestionarios.ubicacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.cuestionarios.ubicacion.QuizUActivity;
import com.example.mundomuisca.menu.MenuActivity;

public class SiguienteUActivity extends AppCompatActivity {

    private int tipo, numPregunta, counterPU, numAciertosU;
    TextView tvCounter, tvMsgPregunta;
    SharedPreferences persistenciaCounterU;
    SharedPreferences.Editor validadorU;
    private Button btnContinuar;
    private boolean validarAcierto;
    LinearLayout linearRecompensas, linearLogro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siguiente_u);

        tvCounter = findViewById(R.id.contador);
        tvMsgPregunta = findViewById(R.id.msgPregunta);
        btnContinuar = findViewById(R.id.btnContinuar);
        linearRecompensas = findViewById(R.id.linearRecompensas);
        linearLogro = findViewById(R.id.linearLogro);

        linearRecompensas.setVisibility(View.GONE);
        linearLogro.setVisibility(View.GONE);

        //TODO: VARIABLES PARA VALIDAR ACIERTO O ERROR DE LA PREGUNTA
        Intent intent = getIntent();
        tipo = intent.getIntExtra("tipo", 0);
        validarAcierto = intent.getBooleanExtra("validarAcierto", false);

        //TODO: VARIABLES PARA LLEVAR CONTEO DE ACIERTOS
        SharedPreferences counterAciertosU = getSharedPreferences("counterAciertosU", Context.MODE_PRIVATE);
        numAciertosU = counterAciertosU.getInt("aciertosU", 0);
        validadorU = counterAciertosU.edit();

        //TODO: VALIDAR SI LA RESPUESTA FUE CORRECTA O INCORRECTA
        if (validarAcierto){
            tvMsgPregunta.setText("RESPUESTA CORRECTA!");
            numAciertosU = numAciertosU + 1;
            validadorU.putInt("aciertosU", numAciertosU);
            validadorU.commit();
        }else {
            tvMsgPregunta.setText("RESPUESTA INCORRECTA!");
        }

        //TODO: VARIABLES PARA LLEVAR EL NUMERO DE PREGUNTAS RESPONDIDAS
        persistenciaCounterU = getSharedPreferences("numPreguntaU", Context.MODE_PRIVATE);
        counterPU = persistenciaCounterU.getInt("counterPreguntasU", 1);
        tvCounter.setText(counterPU + "/10");
        if (counterPU == 10){
            if (numAciertosU > 5){
                SharedPreferences logroU = getSharedPreferences("logros", Context.MODE_PRIVATE);
                SharedPreferences.Editor upLogro = logroU.edit();
                upLogro.putBoolean("logroU", true);
                upLogro.commit();
                tvMsgPregunta.setText("Felicidades!!! " + numAciertosU + "/10");
                linearRecompensas.setVisibility(View.VISIBLE);
                linearLogro.setVisibility(View.VISIBLE);
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(true);
                    }
                });
            }else {
                tvMsgPregunta.setText("Intentalo de nuevo " + numAciertosU + "/10");
                btnContinuar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalizarQuizU(false);
                    }
                });
            }
        }

        SharedPreferences.Editor upCounterPU = persistenciaCounterU.edit();
        //TODO: CAMBIAR PARAMETRO PARA QUE CUENTE EL NUMERO DE PREGUNTAS
        counterPU = counterPU + 1;
        upCounterPU.putInt("counterPreguntasU", counterPU);
        upCounterPU.commit();

    }

    public void siguientePregunta(View view){
        Intent siguiente;
        switch (tipo){
            case 1:
                numPregunta = persistenciaCounterU.getInt("multiple", 0);
                if (numPregunta <= 3) {
                siguiente = new Intent(this, QuizUActivity.class);
                siguiente.putExtra("tipo", 2);
                this.finish();
                startActivity(siguiente);
                }else {
                    siguiente = new Intent(this, QuizUActivity.class);
                    siguiente.putExtra("tipo", 3);
                    this.finish();
                    startActivity(siguiente);
                }
                break;
            case 2:
                numPregunta = persistenciaCounterU.getInt("imagePU", 0);
                if (numPregunta <= 1) {
                siguiente = new Intent(this, QuizUActivity.class);
                siguiente.putExtra("tipo", 3);
                this.finish();
                startActivity(siguiente);
                }else {
                    siguiente = new Intent(this, QuizUActivity.class);
                    siguiente.putExtra("tipo", 4);
                    this.finish();
                    startActivity(siguiente);
                }
                break;
            case 3:
                siguiente = new Intent(this, QuizUActivity.class);
                siguiente.putExtra("tipo", 4);
                this.finish();
                startActivity(siguiente);
                break;
            case 4:
                numPregunta = persistenciaCounterU.getInt("completar", 0);
                if (numPregunta <= 3) {
                    siguiente = new Intent(this, QuizUActivity.class);
                    siguiente.putExtra("tipo", 1);
                    this.finish();
                    startActivity(siguiente);
                }else {
                    siguiente = new Intent(this, QuizUActivity.class);
                    siguiente.putExtra("tipo", 2);
                    this.finish();
                    startActivity(siguiente);
                }
                break;
        }
    }

    public void finalizarQuizU(boolean completado){
        Intent intent = new Intent(this, MenuActivity.class);
        SharedPreferences recompensas = getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorRecompensas = recompensas.edit();
        if (completado) {
            int monedasActuales = recompensas.getInt("monedas", 100);
            int vidasActuales = recompensas.getInt("vidas", 10);
            editorRecompensas.putInt("monedas", monedasActuales + 100);
            editorRecompensas.putInt("vidas", vidasActuales + 3);
            editorRecompensas.putBoolean("recorrido", true);
        }

        SharedPreferences numPreguntaU = getSharedPreferences("numPreguntaU", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = numPreguntaU.edit();
        editor.putInt("completar",1);
        editor.putInt("multiple",1);
        editor.putInt("imagePU",1);
        editor.putInt("truePU",1);
        editor.putInt("counterPreguntasU", 1);
        validadorU.putInt("aciertosU", 0);
        validadorU.apply();
        editor.apply();
        editorRecompensas.apply();
        this.finish();
        startActivity(intent);
    }
}

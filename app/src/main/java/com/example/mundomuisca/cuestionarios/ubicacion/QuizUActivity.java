package com.example.mundomuisca.cuestionarios.ubicacion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.R;

public class QuizUActivity extends AppCompatActivity implements CompletarUFragment.OnFragmentInteractionListener, MultipleUFragment.OnFragmentInteractionListener, ImagenUFragment.OnFragmentInteractionListener, TrueFalseUFragment.OnFragmentInteractionListener{

    private TextView tvMonedas, tvVidas;
    CompletarUFragment completar;
    MultipleUFragment multiple;
    ImagenUFragment imagen;
    TrueFalseUFragment dosOpciones;
    private int tipo, numCompletar, numMultiple, numImage, numTrue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_u);

        completar = new CompletarUFragment();
        multiple =  new MultipleUFragment();
        imagen = new ImagenUFragment();
        dosOpciones = new TrueFalseUFragment();

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);

        //Llamado al metodo cargar logros para que me traiga
        // la informacion almacenada en logros (Vidas - Mondedas)
        cargarLogros();

        Intent intent = getIntent();
        tipo = intent.getIntExtra("tipo", 0);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (Integer.parseInt(tvVidas.getText().toString()) > 0) {
            switch (tipo) {
                case 0:
                    SharedPreferences numPreguntaU = getSharedPreferences("numPreguntaU", Context.MODE_PRIVATE);

                    numCompletar = numPreguntaU.getInt("completar", 0);
                    numMultiple = numPreguntaU.getInt("multiple", 0);
                    numImage = numPreguntaU.getInt("imagePU", 0);
                    numTrue = numPreguntaU.getInt("truePU", 0);
                    if (numCompletar <= 3) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, completar).commit();
                    } else if (numMultiple <= 3) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, multiple).commit();
                    } else if (numImage <= 1) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, imagen).commit();
                    } else if (numTrue <= 3) {
                        getSupportFragmentManager().beginTransaction().add(R.id.contenedorPU, dosOpciones).commit();
                    } else {
                        Intent intnt = new Intent(this, SiguienteUActivity.class);
                        startActivity(intnt);
                    }
                    break;
                case 1:
                    transaction.replace(R.id.contenedorPU, completar).commit();
                    break;
                case 2:
                    transaction.replace(R.id.contenedorPU, multiple).commit();
                    break;
                case 3:
                    transaction.replace(R.id.contenedorPU, imagen).commit();
                    break;
                case 4:
                    transaction.replace(R.id.contenedorPU, dosOpciones).commit();
                    break;
            }
        }else {
                Intent newVidas = new Intent(this, MenuActivity.class);
                newVidas.putExtra("fragmentVidas", true);
                startActivity(newVidas);
                QuizUActivity.this.finish();
        }
    }

    //Metodo para cargar los datos al macenados en logros
    private void cargarLogros(){

        SharedPreferences logros = getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        String modenas = "" + logros.getInt("monedas", 100);
        String vidas =  "" + logros.getInt("vidas", 10);

        tvMonedas.setText(modenas);
        tvVidas.setText(vidas);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

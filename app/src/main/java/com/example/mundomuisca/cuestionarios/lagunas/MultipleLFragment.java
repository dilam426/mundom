package com.example.mundomuisca.cuestionarios.lagunas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.R;
import com.example.mundomuisca.ScreenShot;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.toolbar.Toolbar;


import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MultipleLFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MultipleLFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MultipleLFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    SharedPreferences persistencia_lagunas;

    private Button optionA, optionB, optionC, optionD;

    private View view;

    private Button ayuda50, ayudaAmigo;

    private int numPreguntaM = 0;
    private TextView txtPregunta;

    private TextView tvVidas, tvMonedas;
    private Toolbar toolbar;
    int restaVida;

    private Boolean resta;

    //Activity activity;
    private ScreenShot screenShot;
    private Sonidos sonidos;

    private TextView timer;
    private CountDownTimer countDownTimer;

    private OnFragmentInteractionListener mListener;

    public MultipleLFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MultipleLFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MultipleLFragment newInstance(String param1, String param2) {
        MultipleLFragment fragment = new MultipleLFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_multiple_l, container, false);

        //Creacion de toolbar
        tvVidas = ((QuizLActivity) getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((QuizLActivity) getActivity()).findViewById(R.id.editTextMonedas);
        toolbar = new Toolbar(getContext(), tvMonedas, tvVidas);

        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);

        //Variable para validar el numero de pregunta
        persistencia_lagunas = getActivity().getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
        numPreguntaM = persistencia_lagunas.getInt("multipleLagunas", 1);

        screenShot = new ScreenShot(getActivity(), getContext());
        sonidos = new Sonidos(getContext());

        timer = view.findViewById(R.id.timer);

        timer.setText(String.valueOf(30));

        ayuda50 = view.findViewById(R.id.ayuda50);
        ayudaAmigo = view.findViewById(R.id.ayudaAmigo);
        ayudaAmigo.setClickable(false);
        txtPregunta = view.findViewById(R.id.txtPregunta);
        optionA = view.findViewById(R.id.opcionA);
        optionB = view.findViewById(R.id.opcionB);
        optionC = view.findViewById(R.id.opcionC);
        optionD = view.findViewById(R.id.opcionD);

        //Validacion para agregar el contenido a la vista segun la pregunta
        switch (numPreguntaM) {
            case 1:
                //lAGUNA DE GUASCA
                txtPregunta.setText("¿Quienes causaron el fin de la laguna de Martos?");
                optionA.setText("Los Americanos");
                optionB.setText("Los Portugueses");
                optionC.setText("Los Holandeses");
                optionD.setText("Los Japoneses");
                break;
            case 2:
                //LAGUNA DE SIECHA
                txtPregunta.setText("¿En que año fue encontrada la pieza de oro en forma de balsa?");
                optionA.setText("1942");
                optionB.setText("1856");
                optionC.setText("1925");
                optionD.setText("1430");
                break;
            case 3:
                //LAGUNA DE TEUSACA
                txtPregunta.setText("¿Qué se rumora de la laguna de Teusaca?");
                optionA.setText("En esta laguna había grandes tesoros");
                optionB.setText("La laguna desapareció");
                optionC.setText("En el fondo de la laguna había esmeraldas");
                optionD.setText("En esta laguna los muisca arrojaban a sus difuntos");
                break;
            case 4:
                //LAGUNA DE UBAQUE
                txtPregunta.setText("¿Qué leyendas surgieron de la laguna Ubaque?");
                optionA.setText("El Dorado");
                optionB.setText("El indio dorado y el pozo de Hunzahúa");
                optionC.setText("La china y la búsqueda del tesoro");
                optionD.setText("Mohán y barba blanca");
                break;
        }
        onClick(view);
        starTimer();

        return view;
    }

    private void starTimer() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                sonidos.sonidoError();
                countDownTimer.cancel();
                checkPregunta(numPreguntaM, false);
                actualizarPreguntaM();
                Intent intent = new Intent(getActivity(), SiguienteLActivity.class);
                intent.putExtra("tipo", 2);
                intent.putExtra("validarAcierto", false);
                actualizarPreguntaM();
                restarVidasFragment();
                startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void onClick(View view) {

        //TODO: INTENT PARA PASAR A LA VENTANA DE CORRECTO O INCORRECTO
        final Intent intent = new Intent(getActivity(), SiguienteLActivity.class);
        intent.putExtra("tipo", 2);


        optionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 3) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaM, true);
                    actualizarPreguntaM();
                    optionA.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    checkPregunta(numPreguntaM, false);
                    actualizarPreguntaM();
                    optionA.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    intent.putExtra("validarAcierto", false);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        optionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 2) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaM, true);
                    actualizarPreguntaM();
                    optionB.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    checkPregunta(numPreguntaM, false);
                    actualizarPreguntaM();
                    optionB.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    intent.putExtra("validarAcierto", false);
                    startActivity(intent);
                    getActivity().finish();
                }

            }

        });


        optionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 1) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaM, true);
                    optionC.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    actualizarPreguntaM();

                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();

                } else {
                    sonidos.sonidoError();
                    checkPregunta(numPreguntaM, false);
                    actualizarPreguntaM();
                    intent.putExtra("validarAcierto", false);
                    optionC.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        optionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numPreguntaM == 4) {
                    sonidos.sonidoCorrecto();
                    checkPregunta(numPreguntaM, true);
                    actualizarPreguntaM();
                    optionD.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    intent.putExtra("validarAcierto", true);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    sonidos.sonidoError();
                    checkPregunta(numPreguntaM, false);
                    actualizarPreguntaM();
                    optionD.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    intent.putExtra("validarAcierto", false);
                    restarVidasFragment();
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        ayuda50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoPista();
                    ayuda50.setClickable(false);
                    switch (numPreguntaM) {
                        case 1:
                            optionA.setVisibility(View.INVISIBLE);
                            optionD.setVisibility(View.INVISIBLE);
                            break;
                        case 2:
                            optionA.setVisibility(View.INVISIBLE);
                            optionC.setVisibility(View.INVISIBLE);
                            break;
                        case 3:
                            optionB.setVisibility(View.INVISIBLE);
                            optionD.setVisibility(View.INVISIBLE);
                            break;
                        case 4:
                            optionA.setVisibility(View.INVISIBLE);
                            optionB.setVisibility(View.INVISIBLE);
                            break;
                    }
                } else {
                    toastPersonalizado();
                }
            }
        });

        ayudaAmigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resta = toolbar.restarMonedas();
                if (resta) {
                    sonidos.sonidoScreen();
                    screenShot.ScreenShott(v);
                } else {
                    toastPersonalizado();
                }
            }
        });
    }

    private void checkPregunta(int num, boolean check) {
        SharedPreferences.Editor editor = persistencia_lagunas.edit();
        if (num == 1 && check) {
            editor.putInt("checkP4", 2);
        } else if (num == 1 && !check) {
            editor.putInt("checkP4", 3);
        } else if (num == 2 && check) {
            editor.putInt("checkP6", 2);
        } else if (num == 2 && !check) {
            editor.putInt("checkP6", 3);
        } else if (num == 3 && check) {
            editor.putInt("checkP8", 2);
        } else if (num == 3 && !check) {
            editor.putInt("checkP8", 3);
        } else if (num == 4 && check) {
            editor.putInt("checkP10", 2);
        } else if (num == 4 && !check) {
            editor.putInt("checkP10", 3);
        }
        editor.commit();
    }

    private void actualizarPreguntaM() {
        //TODO: VARIABLES PARA CAMBIAR EL NUMERO DE PREGUNTA
        numPreguntaM = persistencia_lagunas.getInt("multipleLagunas", 0) + 1;
        final SharedPreferences.Editor editor = persistencia_lagunas.edit();
        editor.putInt("multipleLagunas", numPreguntaM);
        editor.commit();

    }

    //TODO: METODO PARA RESTAR VIDAS
    private void restarVidasFragment() {
        restaVida = Integer.parseInt(tvVidas.getText().toString()) - 1;
        toolbar.restarVidas(restaVida);
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("No tienes monedas suficientes!");
        Toast toast = new Toast(getContext().getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
    }
}

package com.example.mundomuisca.toolbar;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mundomuisca.R;

public class LogrosFragment extends Fragment {

    private ImageView logroU, logroR, logroM, logroC;
    private Boolean bolLogroU, bolLogroR, bolLogroM, bolLogroC;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logros, container, false);

        logroU = view.findViewById(R.id.ivLogroU);
        logroR = view.findViewById(R.id.ivLogroR);
        logroM = view.findViewById(R.id.ivLogroM);
        logroC = view.findViewById(R.id.ivLogroC);

        final SharedPreferences totemU = getContext().getSharedPreferences("logros", Context.MODE_PRIVATE);
        bolLogroU = totemU.getBoolean("logroU", false);
        bolLogroR = totemU.getBoolean("logroR", false);
        bolLogroM = totemU.getBoolean("logroM", false);
        bolLogroC = totemU.getBoolean("logroC", false);

        if (bolLogroU){
            logroU.setImageResource(R.drawable.mitad2dorada);
        }else {
            logroU.setImageResource(R.drawable.mitad2gris);
        }
        if (bolLogroR){
            logroR.setImageResource(R.drawable.mitad1dorada);
        }else {
            logroR.setImageResource(R.drawable.mitad1gris);
        }
        if (bolLogroM){
            logroM.setImageResource(R.drawable.izquierdadorada);
        }else {
            logroM.setImageResource(R.drawable.izquierdagris);
        }
        if (bolLogroC){
            logroC.setImageResource(R.drawable.derechadorada);
        }else {
            logroC.setImageResource(R.drawable.derechagris);
        }
        return view;
    }
}

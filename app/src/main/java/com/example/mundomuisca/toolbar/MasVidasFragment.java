package com.example.mundomuisca.toolbar;


import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.R;

import java.util.Random;

public class MasVidasFragment extends Fragment implements Animation.AnimationListener {

    boolean girar = true;
    int intNumber = 6;
    long lngDegrees = 0;
    View view;
    private Sonidos sonidos;
    int numVidas;
    Button btnGirar;
    ImageView selected, imageRuleta;
    TextView tvVidas, tvMonedas;
    Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mas_vidas, container, false);

        btnGirar = view.findViewById(R.id.btnGirar);
        selected = view.findViewById(R.id.flechaRuleta);
        imageRuleta = view.findViewById(R.id.ruleta);

        tvVidas = ((MenuActivity)getActivity()).findViewById(R.id.editTextVidas);
        tvMonedas = ((MenuActivity)getActivity()).findViewById(R.id.editTextMonedas);
        toolbar =  new Toolbar(getContext(), tvMonedas,tvVidas);

        sonidos = new Sonidos(getContext());

        btnGirar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickGirar();
            }
        });


        return view;
    }

    @Override
    public void onAnimationStart(android.view.animation.Animation animation) {
        this.girar = false;
        btnGirar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationEnd(android.view.animation.Animation animation) {

        numVidas = (int)(((double)this.intNumber)
                - Math.floor(((double)this.lngDegrees) / (360.0d / ((double)this.intNumber))));
        toastPersonalizadoVidas(numVidas);
        ((MenuActivity)getActivity()).sumarVida(numVidas + "");
        toolbar.sumarVidasRuleta(numVidas);
        this.girar = true;
    }

    @Override
    public void onAnimationRepeat(android.view.animation.Animation animation) {
    }



    public void onClickGirar(){
        if (tvVidas.getText().toString().equals("0")){
            if (this.girar) {
                sonidos.sonidoRuleta();
                int ran = new Random().nextInt(360) + 3600;
                RotateAnimation rotateAnimation = new RotateAnimation((float) lngDegrees, (float)
                        (lngDegrees + ((long) ran)), 1, 0.5f, 1, 0.5f);
                lngDegrees = (lngDegrees + ((long) ran)) % 360;
                rotateAnimation.setDuration((long) ran);
                rotateAnimation.setFillAfter(true);
                rotateAnimation.setInterpolator(new DecelerateInterpolator());
                rotateAnimation.setAnimationListener(this);
                imageRuleta.setAnimation(rotateAnimation);
                imageRuleta.startAnimation(rotateAnimation);
            }
        }else{
            toastPersonalizado();
        }
    }


    //Método para crear un Toast personalizado
    private void toastPersonalizado(){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("Aún no pierdes todas las vidas!");
        Toast toast = new Toast(getContext().getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    //Método para crear un Toast personalizado
    private void toastPersonalizadoVidas(int vidasGanadas){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("Recompensa: " + vidasGanadas + " vidas!");
        ImageView imageView = Layout.findViewById(R.id.imgIcono);
        imageView.setImageResource(R.drawable.addlives);
        Toast toast = new Toast(getContext().getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }



}

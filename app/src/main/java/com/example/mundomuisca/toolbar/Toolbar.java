package com.example.mundomuisca.toolbar;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Toolbar {

    private TextView tvMonedas, tvVidas;
    Context c;


    public Toolbar(Context context, TextView tvMonedas, TextView tvVidas){
        this.c = context;
        this.tvMonedas = tvMonedas;
        this.tvVidas = tvVidas;
    }

    public void cargarLogros(){
        SharedPreferences logros = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        String modenas = "" + logros.getInt("monedas", 100);
        String vidas =  "" + logros.getInt("vidas", 10);

        tvMonedas.setText(modenas);
        tvVidas.setText(vidas);
    }

    public void sumarVidasRuleta(int vidas){
        SharedPreferences logros = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = logros.edit();
        editor.putInt("vidas", vidas);

        editor.commit();
    }

    public void restarVidas(int restarVida){
        SharedPreferences logros = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = logros.edit();
        editor.putInt("vidas", restarVida);

        editor.commit();
    }

    public Boolean restarMonedas(){
        SharedPreferences logros = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        int monedasActuales = logros.getInt("monedas",100);
        int restar = 0;
        boolean restaMoneda = false;
        if (monedasActuales >= 50) {
            restar = monedasActuales - 50;
            restaMoneda = true;
        }
        tvMonedas.setText("" + restar);
        SharedPreferences.Editor editor = logros.edit();
        editor.putInt("monedas", restar);

        editor.commit();

        return restaMoneda;
    }

}

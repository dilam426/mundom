package com.example.mundomuisca;


import android.content.Context;
import android.media.MediaPlayer;

public class Sonidos {
    private MediaPlayer mp;
    private Context context;


    public Sonidos(Context context) {
        this.context = context;
    }

    public void sonidoCorrecto() {
        mp = MediaPlayer.create(context, R.raw.correct);
        mp.start();
    }

    public void sonidoError() {
        mp = MediaPlayer.create(context, R.raw.error);
        mp.start();
    }

    public void sonidoPista() {
        mp = MediaPlayer.create(context, R.raw.pista);
        mp.start();
    }

    public void sonidoScreen() {
        mp = MediaPlayer.create(context, R.raw.captura);
        mp.start();
    }

    public void sonidoRuleta() {
        mp = MediaPlayer.create(context, R.raw.ruleta);
        mp.start();
    }

    public void sonidoClic() {
        mp = MediaPlayer.create(context, R.raw.clic);
        mp.start();
    }

    public void sonidoFinal() {
        mp = MediaPlayer.create(context, R.raw.sonidocorrecto);
        mp.start();
    }
}


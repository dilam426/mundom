package com.example.mundomuisca.inicio;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.fragments.BienvenidoFragment;
import com.example.mundomuisca.fragments.ContenidoFragment;
import com.example.mundomuisca.fragments.DinamicaFragment;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mundomuisca.menu.MenuActivity;
import com.example.mundomuisca.ui.main.SectionsPagerAdapter;

public class InstruccionesActivity extends AppCompatActivity implements BienvenidoFragment.OnFragmentInteractionListener,
        ContenidoFragment.OnFragmentInteractionListener, DinamicaFragment.OnFragmentInteractionListener {
    ViewPager viewPager;
    private LinearLayout linearPuntos;
    private TextView[] puntosSlide;
    Sonidos sonidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instrucciones);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        linearPuntos = findViewById(R.id.idLinearPuntos);
        agregarPuntos(0);
        viewPager.addOnPageChangeListener(viewListener);
        sonidos = new Sonidos(this);
    }

    //Intent para enviar al menu
    public void inicio(View view) {
        sonidos.sonidoClic();
        Intent menu = new Intent(this, MenuActivity.class);
        startActivity(menu);
        finish();
    }

    //Metodo que pinta los botones que se encuentran arriba del boton
    private void agregarPuntos(int p) {

        puntosSlide = new TextView[3];
        linearPuntos.removeAllViews();

        for (int i=0; i<puntosSlide.length; i++){
            puntosSlide[i] = new TextView(this);
            puntosSlide[i].setText(Html.fromHtml("&#8226"));
            puntosSlide[i].setTextSize(45);
            puntosSlide[i].setTextColor(getResources().getColor(R.color.colorTransparente));
            linearPuntos.addView(puntosSlide[i]);
        }

        if(puntosSlide.length>0){
            puntosSlide[p].setTextColor(getResources().getColor(R.color.colorOscuro));
        }

    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            agregarPuntos(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}
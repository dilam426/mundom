package com.example.mundomuisca.menu;

public class ModelOpcionesMenu {

    //Clase que define los elementos que contiene el componente cardView

    private String titulo, descripcion;
    private int logoImg, nivelImg;

    public ModelOpcionesMenu(int logoImg, String titulo, String descripcion, int nivelImg) {
        this.logoImg = logoImg;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.nivelImg = nivelImg;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getLogoImg() {
        return logoImg;
    }

    public void setLogoImg(int logoImg) {
        this.logoImg = logoImg;
    }

    public int getNivelImg() {
        return nivelImg;
    }

    public void setNivelImg(int nivelImg) {
        this.nivelImg = nivelImg;
    }
}

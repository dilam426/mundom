package com.example.mundomuisca.menu;

import android.view.View;

public interface ItemClickListenerMenu {
    void onItemClickListener(View v, int position);
}

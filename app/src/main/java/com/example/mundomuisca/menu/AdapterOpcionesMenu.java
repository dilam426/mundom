package com.example.mundomuisca.menu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mundomuisca.R;
import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.cuestionarios.costumbres.CostumbresActivity;
import com.example.mundomuisca.cuestionarios.lagunas.LagunasActivity;
import com.example.mundomuisca.cuestionarios.mitologia.MitologiaActivity;
import com.example.mundomuisca.cuestionarios.ubicacion.UbicacionActivity;

import java.util.ArrayList;

//Definición de la clase Adapter; esta clase crea un cardview y lo pobla con sus respectivos datos

public class AdapterOpcionesMenu extends RecyclerView.Adapter<HolderOpcionesMenu>{

    Context c;
    View view;
    private ArrayList<ModelOpcionesMenu> dataSet;
    Sonidos sonidos;

    public AdapterOpcionesMenu(Context c, ArrayList<ModelOpcionesMenu> datos) {
        this.c = c;
        this.dataSet = datos;
    }

    @NonNull
    @Override

    //Crea el card view dentro de un layout
    public HolderOpcionesMenu onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //El LayoutInflater me da una referencia a un layout y a traves de esa clase agregarle un elemento
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_menu, parent, false);
        sonidos = new Sonidos(c);
        return new HolderOpcionesMenu(view);
    }

    @Override
    //Llenado del card view
    public void onBindViewHolder(@NonNull HolderOpcionesMenu holder, int position) {
        ModelOpcionesMenu modelOpcionesMenu = dataSet.get(position);
        holder.logoView.setImageResource(modelOpcionesMenu.getLogoImg());
        holder.tituloText.setText(modelOpcionesMenu.getTitulo());
        holder.descripcionText.setText(modelOpcionesMenu.getDescripcion());
        holder.nivelView.setImageResource(modelOpcionesMenu.getNivelImg());



        // Método para la escucha del clic sobre las opciones del menú
        holder.setItemClickListener(new ItemClickListenerMenu() {


            @Override
            public void onItemClickListener(View v, int position) {
                sonidos.sonidoClic();
                if(dataSet.get(position).getDescripcion().equals("Nivel 1")) {
                    Intent intent = new Intent(c, UbicacionActivity.class);
                    c.startActivity(intent);
                }
                if(dataSet.get(position).getDescripcion().equals("Nivel 2")) {
                    //Declaracion del shares preferences para validar si el nivel se encuentra desbloqueado
                    SharedPreferences preferences = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);
                    boolean estado = preferences.getBoolean("recorrido",false);
                    if (!estado){
                        toastPersonalizado();
                    }else {
                        Intent intent = new Intent(c, LagunasActivity.class);
                        c.startActivity(intent);
                    }
                }
                if(dataSet.get(position).getDescripcion().equals("Nivel 3")) {
                    SharedPreferences preferences = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);
                    boolean estado = preferences.getBoolean("mitologia",false);
                    if (!estado){
                        toastPersonalizado();
                    }else {
                        Intent intent = new Intent(c, MitologiaActivity.class);
                        c.startActivity(intent);
                    }
                }
                if(dataSet.get(position).getDescripcion().equals("Nivel 4")) {
                    SharedPreferences preferences = c.getSharedPreferences("logrosO", Context.MODE_PRIVATE);
                    boolean estado = preferences.getBoolean("costumbres",false);
                    if (!estado){
                        toastPersonalizado();
                    }else {
                        Intent intent = new Intent(c, CostumbresActivity.class);
                        c.startActivity(intent);
                    }
                }
            }
        });
    }



    @Override
    //Cantidad de elementos del Dataset
    public int getItemCount() {
        return dataSet.size();
    }


    //Método para crear un Toast personalizado
    public void toastPersonalizado(){
        LayoutInflater inflater = (LayoutInflater) c.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) view.findViewById(R.id.ToastNivel));
        ImageView imageView = Layout.findViewById(R.id.imgIcono);
        imageView.setImageResource(R.drawable.lock);
        Toast toast = new Toast(c.getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

}

package com.example.mundomuisca.menu;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mundomuisca.R;

//En esta clase se definen todos los elementos que se quieren personalizar por cada cardView; es la referencia de ese componente.

public class HolderOpcionesMenu extends RecyclerView.ViewHolder implements View.OnClickListener{

    public ImageView logoView, nivelView;
    public TextView tituloText, descripcionText;
    public ItemClickListenerMenu itemClickListenerMenu;


    public HolderOpcionesMenu(@NonNull View itemView) {
        super(itemView);
        this.logoView = itemView.findViewById(R.id.logoPrincipal);
        this.tituloText = itemView.findViewById(R.id.titulo);
        this.descripcionText = itemView.findViewById(R.id.descripcion);
        this.nivelView = itemView.findViewById(R.id.candado);

        itemView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        this.itemClickListenerMenu.onItemClickListener(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListenerMenu ic){
        this.itemClickListenerMenu = ic;
    }
}

package com.example.mundomuisca.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mundomuisca.Sonidos;
import com.example.mundomuisca.inicio.InicioFragment;
import com.example.mundomuisca.R;
import com.example.mundomuisca.toolbar.LogrosFragment;
import com.example.mundomuisca.toolbar.MasVidasFragment;
import com.example.mundomuisca.toolbar.Toolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    private ArrayList<ModelOpcionesMenu> dataSetModel;
    public BottomNavigationView bottomNavigationView;
    public RecyclerView recyclerView;
    Sonidos sonidos;

    private Button reStart;

    TextView tvMonedas, tvVidas, LblToastCustom;
    Toolbar toolbar;

    private SharedPreferences preferences;

    private LinearLayout overBox;
    private ConstraintLayout contenedorAlert;
    private ObjectAnimator aTtotem1, aTotem2, aTotem3, aTotem4;
    private ImageView totem1, totem2, totem3, totem4;

    //Layouts contendeor de la alerta y contenedor para el color de fondo oscuro
    LinearLayout contenidoAnuncio;

    //Logo de la alerta
    ConstraintLayout estatuilla;

    //Animación para la alerta
    Animation fromsmall, fromnothing, fromAdverImg, togo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        tvMonedas = findViewById(R.id.editTextMonedas);
        tvVidas = findViewById(R.id.editTextVidas);
        toolbar = new Toolbar(this, tvMonedas, tvVidas);
        LblToastCustom = findViewById(R.id.LblToastCustom);
        toolbar.cargarLogros();
        sonidos = new Sonidos(this);

        preferences = getSharedPreferences("logrosO", Context.MODE_PRIVATE);

        overBox = findViewById(R.id.overbox);
        contenidoAnuncio = findViewById(R.id.contenidoBox);
        contenedorAlert = findViewById(R.id.contenedorAlert);
        estatuilla = findViewById(R.id.contenedorEstatuilla);
        fromsmall = AnimationUtils.loadAnimation(this, R.anim.fromsmall);
        fromnothing = AnimationUtils.loadAnimation(this, R.anim.fromnothing);
        fromAdverImg = AnimationUtils.loadAnimation(this, R.anim.from_adver_img);
        togo = AnimationUtils.loadAnimation(this, R.anim.togo);
        totem1 = findViewById(R.id.estatuilla1);
        totem2 = findViewById(R.id.estatuilla2);
        totem3 = findViewById(R.id.estatuilla3);
        totem4 = findViewById(R.id.estatuilla4);
        finJuego(preferences.getBoolean("final", false));

        reStart = findViewById(R.id.closeAviso);

        //Declaro el componente Bottom Navigation Bar
        bottomNavigationView = findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        //Declaro el componente Recycler view
        recyclerView = findViewById(R.id.recyclerViewMenu);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        dataSetModel = new ArrayList<>();
        llenarDataSet();

        recyclerView.setAdapter(new AdapterOpcionesMenu(this, dataSetModel));

        Intent i = getIntent();
        if (i.getBooleanExtra("fragmentVidas", false)) {
            toastPersonalizado();
            Fragment vidasFragment = new MasVidasFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frameBottomNavigation, vidasFragment).commit();
        }

    }

    //Método para escuchar el evento sobre las opciones del menu del Bottom Navigation Bar
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    sonidos.sonidoClic();
                    switch (item.getItemId()) {
                        case R.id.itemLogros:
                            selectedFragment = new LogrosFragment();
                            break;

                        case R.id.itemMasVidas:
                            selectedFragment = new MasVidasFragment();
                            break;

                        case R.id.itemHome:
                            selectedFragment = new InicioFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameBottomNavigation, selectedFragment).commit();
                    return true;
                }
            };

    //Método para llenar el arreglo dataSetModel con la información de cada una de las opciones del menú
    private void llenarDataSet() {
        dataSetModel.add(new ModelOpcionesMenu(R.drawable.mapa, "Ubicación Geográfica", "Nivel 1", R.drawable.niveldesbloqueado));
        if (preferences.getBoolean("recorrido", false)) {
            dataSetModel.add(new ModelOpcionesMenu(R.drawable.laguna, "Recorrido Sagrado", "Nivel 2", R.drawable.niveldesbloqueado));
        } else {
            dataSetModel.add(new ModelOpcionesMenu(R.drawable.laguna, "Recorrido Sagrado", "Nivel 2", R.drawable.nivelbloqueado));
        }
        if (preferences.getBoolean("mitologia", false)) {
            dataSetModel.add(new ModelOpcionesMenu(R.drawable.mitos, "Mitología Muisca", "Nivel 3", R.drawable.niveldesbloqueado));
        } else {
            dataSetModel.add(new ModelOpcionesMenu(R.drawable.mitos, "Mitología Muisca", "Nivel 3", R.drawable.nivelbloqueado));
        }
        if (preferences.getBoolean("costumbres", false)) {
            dataSetModel.add(new ModelOpcionesMenu(R.drawable.costumbres, "Costumbres y" + "\n" + "Modos de Vida", "Nivel 4", R.drawable.niveldesbloqueado));
        }else {
            dataSetModel.add(new ModelOpcionesMenu(R.drawable.costumbres, "Costumbres y" + "\n" + "Modos de Vida", "Nivel 4", R.drawable.nivelbloqueado));
        }
    }

    public void sumarVida(String vidas) {
        int suma = Integer.parseInt(vidas) + Integer.parseInt(tvVidas.getText().toString());
        tvVidas.setText(String.valueOf(suma));
    }

    //Método para crear un Toast personalizado
    public void toastPersonalizado() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Layout = inflater.inflate(R.layout.toast_personalizado, (ViewGroup) findViewById(R.id.ToastNivel));
        TextView mensaje = Layout.findViewById(R.id.LblToastCustom);
        mensaje.setText("Has perdido todas las vidas!");
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 210);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(Layout);
        toast.show();
    }

    private void finJuego(Boolean isEnd){
        if (!isEnd){
            contenedorAlert.setVisibility(View.INVISIBLE);
            overBox.setVisibility(View.INVISIBLE);
        }else {
            sonidos.sonidoFinal();
            //Animacion Totem1
            aTtotem1 = ObjectAnimator.ofFloat(totem1, View.ALPHA, 0.0f, 1.0f);
            aTtotem1.setDuration(5000);
            AnimatorSet animatorSet1 = new AnimatorSet();
            animatorSet1.play(aTtotem1);
            animatorSet1.start();


            //Animacion Totem2
            aTotem2 = ObjectAnimator.ofFloat(totem2, View.ALPHA, 0.0f, 1.0f);
            aTotem2.setDuration(8000);
            AnimatorSet animatorSet2 = new AnimatorSet();
            animatorSet2.play(aTotem2);
            animatorSet2.start();

            //Animacion Totem3
            aTotem3 = ObjectAnimator.ofFloat(totem3, View.ALPHA, 0.0f, 1.0f);
            aTotem3.setDuration(11000);
            AnimatorSet animatorSet3 = new AnimatorSet();
            animatorSet3.play(aTotem3);
            animatorSet3.start();

            //Animacion Totem4
            aTotem4 = ObjectAnimator.ofFloat(totem4, View.ALPHA, 0.0f, 1.0f);
            aTotem4.setDuration(15000);
            AnimatorSet animatorSet4 = new AnimatorSet();
            animatorSet4.play(aTotem4);
            animatorSet4.start();
        }
    }

    public void reStartGame(View view){

        //Cerrar Anuncio de la estatuilla
        overBox.startAnimation(togo);
        estatuilla.startAnimation(togo);
        contenidoAnuncio.startAnimation(togo);
        estatuilla.setVisibility(View.GONE);
        ViewCompat.animate(contenidoAnuncio).setStartDelay(1000).alpha(0).start();
        ViewCompat.animate(overBox).setStartDelay(1000).alpha(0).start();

        tvVidas.setText("10");
        tvMonedas.setText("100");

        //TODO: VARIABLES PARA LLEVAR CONTEO DE ACIERTOS Y ERRORES
        SharedPreferences counterAciertosU = getSharedPreferences("counterAciertosU", Context.MODE_PRIVATE);
        SharedPreferences.Editor resetAciertos = counterAciertosU.edit();

        SharedPreferences logros = getSharedPreferences("logrosO", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = logros.edit();
        editor.putInt("monedas", 100);
        editor.putInt("vidas", 10);
        editor.putBoolean("Ingreso", false);
        editor.putBoolean("IngresoC", false);
        editor.putBoolean("final", false);

        final SharedPreferences totemU = getSharedPreferences("logros", Context.MODE_PRIVATE);
        SharedPreferences.Editor resetLogros = totemU.edit();
        resetLogros.putBoolean("logroU", false);
        resetLogros.putBoolean("logroR", false);
        resetLogros.putBoolean("logroM", false);
        resetLogros.putBoolean("logroC", false);

        SharedPreferences persistenciaLagunas = getSharedPreferences("PERSISTENCIA_LAGUNAS", Context.MODE_PRIVATE);
        SharedPreferences.Editor resetLagunas = persistenciaLagunas.edit();
        resetLagunas.putInt("completarLagunas", 1);
        resetLagunas.putInt("multipleLagunas", 1);
        resetLagunas.putInt("imagenLagunas", 1);
        resetLagunas.putInt("trueLagunas", 1);
        resetLagunas.putInt("counterPreguntasL", 1);
        resetLagunas.putInt("numQuestion", 0);
        resetLagunas.putInt("numInfo",1);
        resetLagunas.putInt("aciertosL", 0);
        resetLagunas.putInt("checkP1", 1);
        resetLagunas.putInt("checkP2", 1);
        resetLagunas.putInt("checkP3", 1);
        resetLagunas.putInt("checkP4", 1);
        resetLagunas.putInt("checkP5", 1);
        resetLagunas.putInt("checkP6", 1);
        resetLagunas.putInt("checkP7", 1);
        resetLagunas.putInt("checkP8", 1);
        resetLagunas.putInt("checkP9", 1);
        resetLagunas.putInt("checkP10", 1);
        resetLagunas.apply();

        SharedPreferences persistencia_mitos = getSharedPreferences("PERSISTENCIA_MITOS", Context.MODE_PRIVATE);
        SharedPreferences.Editor resetMitos = persistencia_mitos.edit();
        resetMitos.putInt("completarMitos", 1);
        resetMitos.putInt("multipleMitos", 1);
        resetMitos.putInt("imagenMitos", 1);
        resetMitos.putInt("trueMitos", 1);
        resetMitos.putInt("counterPreguntasM", 1);
        resetMitos.putInt("aciertosM", 0);
        resetMitos.apply();

        SharedPreferences persistencia_costumbres = getSharedPreferences("PERSISTENCIA_COSTUMBRES", Context.MODE_PRIVATE);
        SharedPreferences.Editor resetCostumbres = persistencia_costumbres.edit();
        resetCostumbres.putInt("completarCostumbres", 1);
        resetCostumbres.putInt("multipleCostumbres", 1);
        resetCostumbres.putInt("imagenCostumbres", 1);
        resetCostumbres.putInt("trueCostumbres", 1);
        resetCostumbres.putInt("counterPreguntasC", 1);
        resetCostumbres.putInt("aciertosC", 0);
        resetCostumbres.apply();

        SharedPreferences numPreguntaU = getSharedPreferences("numPreguntaU", Context.MODE_PRIVATE);
        SharedPreferences.Editor resetPreguntas = numPreguntaU.edit();
        resetPreguntas.putInt("completar",1);
        resetPreguntas.putInt("multiple",1);
        resetPreguntas.putInt("imagePU",1);
        resetPreguntas.putInt("truePU",1);
        resetPreguntas.putInt("counterPreguntasU", 1);
        resetAciertos.putInt("aciertosU", 0);
        resetAciertos.commit();
        resetPreguntas.commit();
        resetLogros.commit();
        editor.apply();
    }



    @Override
    protected void onPause() {
        super.onPause();
        MenuActivity.this.finish();
    }
}

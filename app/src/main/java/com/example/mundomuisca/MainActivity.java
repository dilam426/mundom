package com.example.mundomuisca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mundomuisca.inicio.InstruccionesActivity;


public class MainActivity extends AppCompatActivity {
    private TextView tv;
    private ImageView iv;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Declaro el componente toolbar como el actionBar de la app
        toolbar = findViewById(R.id.includeToolbar);
        setSupportActionBar(toolbar);


        tv = findViewById(R.id.nombreApp);
        iv = findViewById(R.id.logoMuisca);

        //Animacion al iniciar la app
        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);

        final Intent i = new Intent(this, InstruccionesActivity.class);

        tv.startAnimation(fadeIn);
        iv.startAnimation(fadeIn);

        //Hilo para pasar a las instrucciones a los 5 segundos
        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }
        };
        timer.start();

        //prueba commit
        //Declaro el componente toolbar como el actionBar de la app
        toolbar = findViewById(R.id.includeToolbar);
        setSupportActionBar(toolbar);


        tv = findViewById(R.id.nombreApp);
        iv = findViewById(R.id.logoMuisca);

    }
}
